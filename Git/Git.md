# 	一、Git概述

Git是一个免费的、开源的**分布式版本控制系统**，可以快速高效地处理从小型到大型的各种项目。

Git易于学习，占地面积小，性能极快。它具有廉价的本地库，方便的暂存区域和多个工作流分支等特性。其性能优于Subversion、

CVS、Perforce 和ClearCase等版本控制工具。

## 1.1、何为版本控制

版本控制是一种记录文件内容变化，以便将来查阅特定版本修订情况的系统。

版本控制其实最重要的是可以记录文件修改历史记录，从而让用户能够查看历史版本,方便版本切换。

![image-20210719105510083](https://gitee.com/feng2828/image/raw/master/img/20210723135840.png)



## 1.2、为什么需要版本控制

**个人开发过渡到团队协作**

![image-20210719110037482](https://gitee.com/feng2828/image/raw/master/img/20210723140408.png)



## 1.3、版本控制工具

> 集中式版本控制工具

**CVS、`SVN(Subversion)`、 VSS...**...

​		集中化的版本控制系统诸如CVS、SVN等，都有一个单一的集中管理的服务器，保存所有文件的修订版本，而协同工作的人们都通过

客户端连到这台服务器,取出最新的文件或者提交更新。多年以来，这已成为版本控制系统的标准做法。

​		这种做法带来了许多好处，每个人都可以在一定程度上看到项目中的其他人正在做些什么。而管理员也可以轻松掌控每个开发者

的权限，并且管理一个集中化的版本控制系统， 要远比在各个客户端上维护本地数据库来得轻松容易

​		事分两面，有好有坏。这么做显而易见的缺点是中央服务器的单点故障。如果服务器宕机一小时，那么在这一小时内，谁都无法提交更新，也就无法协同工作。

![image-20210719110619289](https://gitee.com/feng2828/image/raw/master/img/20210723135936.png)



> 分布式版本控制工具

`Git`、Mercurial、 Bazaar、 Darc.....

​		像Git这种分布式版本控制工具，客户端提取的不是最新版本的文件快照，而是把代码仓库完整地镜像下来(本地库)。这样任何一

处协同工作用的文件发生故障，事后都可以用其他客户端的本地仓库进行恢复。因为每个客户端的每一次文件提取操作,实际上都是一

次对整个文件仓库的完整备份。

分布式的版本控制系统出现之后，解决了集中式版本控制系统的缺陷:

1.服务器断网的情况下也可以进行开发(因为版本控制是在本地进行的)

2.每个客户端保存的也都是整个完整的项目(包含历史记录，更加安全) 

![image-20210719111659423](https://gitee.com/feng2828/image/raw/master/img/20210723140451.png)



> 两种区别

分布式能在本地进行版本控制，集中式不能。

分布式每个人的电脑上都可以直接拷贝完整的代码版本。而集中式只能拷贝自己需要的。

分布式的服务器挂掉之后，不会影响工作。而集中式的服务器挂掉之后，根本就没法进行工作。

当然，Git的优势不单是不必联网这么简单，后面我们还会看到Git极其强大的分支管理，把SVN等远远抛在了后面



## 1.4、Git简史

![image-20210719112136121](https://gitee.com/feng2828/image/raw/master/img/20210723140607.png)





## 1.5、Git工作机制

![image-20210719112848681](https://gitee.com/feng2828/image/raw/master/img/20210723140618.png)

## 1.6、Git和代码托管中心

代码托管中心是基于网络服务器的远程代码仓库，一般我们简单称为==远程库==。

> 局域网

**GitLab**

> 互联网

**GitHub(外网)**

**Gitee码云（国内网站）**



# 二、Git安装

官网地址：https://git-scm.com/

下载：https://git-scm.com/download/win



查看GNU协议，可以直接点击下一步。

![image-20210719114411510](https://gitee.com/feng2828/image/raw/master/img/20210723140632.png)



![image-20210719114642653](https://gitee.com/feng2828/image/raw/master/img/20210723140651.png)

​				**Git选项配置，推荐默认配置，然后下一步**

![image-20210719115651857](https://gitee.com/feng2828/image/raw/master/img/20210723140656.png)





![image-20210719115145364](https://gitee.com/feng2828/image/raw/master/img/20210723140703.png)

**选择默认编辑器为Vim**

![image-20210719115803461](https://gitee.com/feng2828/image/raw/master/img/20210723140710.png)

创建新库是分支名字由Git决定，还是自己决定，我们选Git

![image-20210719120050428](https://gitee.com/feng2828/image/raw/master/img/20210723140718.png)

修改环境变量

![image-20210719120252142](https://gitee.com/feng2828/image/raw/master/img/20210723140723.png)

选择客户端连接协议 ：使用OpenSSL协议进行连接

![image-20210719120440928](https://gitee.com/feng2828/image/raw/master/springboot/20210723141615.png)



换行符转换为windows的

![image-20210719120657323](https://gitee.com/feng2828/image/raw/master/springboot/20210723141621.png)

在Git Bash里用什么终端

![image-20210719121021327](https://gitee.com/feng2828/image/raw/master/springboot/20210723141624.png)

选择关于 Git Pull （从远程库拉取代码到本地时代码的合并模式） 的默认行为

![image-20210719121224708](https://gitee.com/feng2828/image/raw/master/springboot/20210723141629.png)

![image-20210719121302091](https://gitee.com/feng2828/image/raw/master/springboot/20210723141633.png)

其他配置，选择默认

![image-20210719121459991](https://gitee.com/feng2828/image/raw/master/springboot/20210723141637.png)

实验室功能，不成熟，可能有BUG

![image-20210719121827719](https://gitee.com/feng2828/image/raw/master/springboot/20210723141646.png)

点击Finish,安装完成

![image-20210719122019648](https://gitee.com/feng2828/image/raw/master/img/20210723141817.png)

桌面右键，发现有Git Bash 和 Git Gui 选项

![image-20210719122546269](https://gitee.com/feng2828/image/raw/master/img/20210723141822.png)

点击 Git Bash 我们可以输入命令

![image-20210719122505148](https://gitee.com/feng2828/image/raw/master/img/20210723141833.png)

**安装成功！**



# 三、Git 常用命令

![image-20210719122642233](https://gitee.com/feng2828/image/raw/master/img/20210723141839.png)



## 3.1、设置用户签名

**1）基本语法**

```java
git config --global user.name '你的用户名'

git config --global user.email '你的邮箱'
    
cat ~/.gitconfig  //查看配置

```

**2）配置**

![image-20210719124807168](https://gitee.com/feng2828/image/raw/master/img/20210723141843.png)

**3）查看是否配置好**

通过这个路径下的  .gitconfig 文件查看

![image-20210719125655582](https://gitee.com/feng2828/image/raw/master/img/20210723141849.png)

![image-20210719125849222](https://gitee.com/feng2828/image/raw/master/img/20210723141853.png)

也可以通过命令来查询

![image-20210719125922997](https://gitee.com/feng2828/image/raw/master/img/20210723141857.png)



`说明`: 

签名的作用是区分不同操作者身份。用户的签名信息在每一一个版本的提交信息中能够看到，以此确认本次提交是谁做的。

Git首次安装必须设置一下用户 签名,否则无法提交代码。

`注意`: 这里设置用户签名和将来登录GitHub(或其他代码托管中心)的账号没有任何关系。



## 3.2、初始化本地库

**1）基本语法**

```
git init 
```

初始化就是通过git管理项目目录

**2)实际操作**

在需要管理的项目目录里右键，通过命令初始化

![image-20210719143918681](https://gitee.com/feng2828/image/raw/master/img/20210723141902.png)

**3）结果**

会产生一个 .git 文件

![image-20210719144017627](https://gitee.com/feng2828/image/raw/master/img/20210723141907.png)

或者通过命令查询 

```
ll -a
```

![image-20210719144139962](https://gitee.com/feng2828/image/raw/master/img/20210723141910.png)



## 3.3、查看本地库状态

**1）命令**

```
git status
```

![image-20210719144556364](https://gitee.com/feng2828/image/raw/master/img/20210723141914.png)

第一行：在 master 分支

第二行：没有提交过

第三行 : 没有东西提交。空仓库



**Ctrl + L 清屏**



**2）通过vim编辑器创建一个hello.txt 文件**

![image-20210719145635688](https://gitee.com/feng2828/image/raw/master/img/20210723141919.png)

按 i 进入插入模式，Esc 退出

![image-20210719145523641](https://gitee.com/feng2828/image/raw/master/img/20210723141923.png)

![image-20210719145646679](https://gitee.com/feng2828/image/raw/master/img/20210723141928.png)



在vim 里将光标放在第一行， YY 复制 ，再按  P 键复制 

![image-20210719145749425](https://gitee.com/feng2828/image/raw/master/img/20210723141932.png)



按  ：wq  保存退出 或者 shift+z+z

![image-20210719150153900](https://gitee.com/feng2828/image/raw/master/img/20210723141936.png)

这样就创建好了

![image-20210719150218674](https://gitee.com/feng2828/image/raw/master/img/20210723141945.png)

![image-20210719150259774](https://gitee.com/feng2828/image/raw/master/img/20210723141949.png)



**3)这个时候再查看状态**

![image-20210719150522279](https://gitee.com/feng2828/image/raw/master/img/20210723141952.png)

不一样了，第三行：没有追踪的文件   ，也就是这个文件还在工作区



## 3.4、添加暂存区

### 1.将工作区的文件添加到暂存区

命令 

```
git add  文件名.后缀
```

![image-20210719151205132](https://gitee.com/feng2828/image/raw/master/img/20210723141956.png)

这里有个警告： Linux的LF 换行符会被替换成Window的换行符 CRLF



### 2.查看状态（检测到暂存区有新文件）

![image-20210719151451888](https://gitee.com/feng2828/image/raw/master/img/20210723142000.png)

这个文件从红色变绿色了，说明已经在暂存区了



**删除暂存区文件**

```
git rm --cached 文件名.后缀
```

![image-20210719152238573](https://gitee.com/feng2828/image/raw/master/img/20210723142005.png)

==这个只是删除了暂存区的文件，工作区的还在==

![image-20210719152404205](https://gitee.com/feng2828/image/raw/master/img/20210723142008.png)



## 3.5、提交本地库

### 1.将暂存区的文件提交到本地库

**1）语法**

```java
git commmit -m "日志信息" 文件名.后缀
```

2)实操

![image-20210719153111625](https://gitee.com/feng2828/image/raw/master/img/20210723142018.png)



### 2.查看状态（没有文件需要提交）

![image-20210719153340046](https://gitee.com/feng2828/image/raw/master/img/20210723142022.png)

没有东西提交了，工作树干净



**查看本地仓库的文件版本信息**

**1）**第一种

```java
git reflog
```

![image-20210719153654839](https://gitee.com/feng2828/image/raw/master/img/20210723142028.png)

ece5d26  是版本号

**2）**第二种

```
git log	
```

这是查看详细的日志信息

![image-20210719153900688](https://gitee.com/feng2828/image/raw/master/img/20210723142032.png)



## 3.6、修改文件（hello.txt）

这里修改了第一行，添加了一些 22222 ，wq 保存

![image-20210719154637738](https://gitee.com/feng2828/image/raw/master/img/20210723142037.png)



### 3.6.1、查看状态（检测工作区有文件被修改）

![image-20210719154805046](https://gitee.com/feng2828/image/raw/master/img/20210723142046.png)

`modified` 被修改



### 3.6.2、将修改的文件再次添加到暂存区

![image-20210719154924110](https://gitee.com/feng2828/image/raw/master/img/20210723142053.png)

**查看状态（工作区文件的修改添加到了暂存区）**

![image-20210719155008398](https://gitee.com/feng2828/image/raw/master/img/20210723142057.png)



### 3.6.3、再次提交到本地库

![image-20210719155306868](https://gitee.com/feng2828/image/raw/master/img/20210723142100.png)

**这里提示 ：一个文件改变，一行插入，一行删除**

**因为在 Git  里面是按照行来维护文件的，所以改变一行就相当于新增一行，删除了原来的**



这个时候再查看本地库状态，又是干净的了

![image-20210719155915106](https://gitee.com/feng2828/image/raw/master/img/20210723142104.png)

`注意`这个蓝色的 HEAD 指针在的地方就是哪个版本在本地库里



## 3.7、历史版本穿梭

### 3.7.1、查看历史版本

**1）基本语法**

```java
git reflog    // 查看版本信息

git log      // 查看版本详细信息
```



**2）例子**

![image-20210719160814306](https://gitee.com/feng2828/image/raw/master/img/20210723142118.png)



### 3.7.2、版本穿梭（切换）

**1）语法**

```
git reset --hard 版本号
```

**2）实操**

![image-20210719161709449](https://gitee.com/feng2828/image/raw/master/img/20210723142124.png)

**3）通过文件查看版本**

可以通过head 文件查看版本 在 master 文件里

![image-20210719161930802](https://gitee.com/feng2828/image/raw/master/img/20210723142128.png)

![image-20210719162036495](https://gitee.com/feng2828/image/raw/master/img/20210723142135.png)

![image-20210719162156124](https://gitee.com/feng2828/image/raw/master/img/20210723142139.png)



`原理`：Git切换版本，底层其实是移动的HEAD指针，具体原理如下图所示。

![image-20210719163113294](https://gitee.com/feng2828/image/raw/master/img/20210723142144.png)





# 四、Git 分支操作

![image-20210719183406413](https://gitee.com/feng2828/image/raw/master/img/20210723142148.png)



## 4.1、什么是分支

​		在版本控制过程中，同时推进多个任务，为每个任务，我们就可以创建每个任务的单独分支。使用分支意味着程序员可以把自己

的工作从开发主线上分离开来,开发自己分支的时候，不会影响主线分支的运行。对于初学者而言，分支可以简单理解为副本，一个分

支就是一个单独的副本。(分支底层其实也是指针的引用)

![image-20210719184036539](https://gitee.com/feng2828/image/raw/master/img/20210723142152.png)



## 4.2 分支的好处

同时并行推进多个功能开发，提高开发效率。

各个分支在开发过程中，如果某一个分支开发失败，不会对其他分支有任何影响。失败的分支删除重新开始即可。



## 4.3、分支的操作

![image-20210719184410046](https://gitee.com/feng2828/image/raw/master/img/20210723142157.png)

### 4.3.1、查看分支

**1）基本语法**

```
git branch -v
```

**2)实际操作**

![image-20210719184644923](https://gitee.com/feng2828/image/raw/master/img/20210723142201.png)

### 4.3.2、创建分支

**1)基本语法**

```
git branch 分支名
```

**2）实际操作**

![image-20210719185041866](https://gitee.com/feng2828/image/raw/master/img/20210723142205.png)

### 4.3.3、切换分支

**1）语法**

```
git checkout 分支名
```

**2）实际操作**

![image-20210719185524845](https://gitee.com/feng2828/image/raw/master/img/20210723142209.png)



### 4.3.4、在分支上修改文件并且提交到本地库

![image-20210719190309741](https://gitee.com/feng2828/image/raw/master/img/20210723142211.png)

![image-20210719190400530](https://gitee.com/feng2828/image/raw/master/img/20210723142215.png)

### 4.3.5、合并分支

`注意`：这个是正常合并，就是在master分支没变的基础上，进行合并

**1）基本语法**

```
git merge 分支名
```

切换回主分支，发现文件并未修改

![image-20210719190824783](https://gitee.com/feng2828/image/raw/master/img/20210723142218.png)

这个时候我们就要合并分支

**2）案例实操**

**在 master 分支上合并 hot-fix 分支**

![image-20210719191145247](https://gitee.com/feng2828/image/raw/master/img/20210723142226.png)



### 4.3.6、合并产生冲突

**1.修改两个分支的文文件**

**①master分支的文件**

![image-20210719195240386](https://gitee.com/feng2828/image/raw/master/img/20210723142231.png)

**②hot-fix 分支的文件**

![image-20210719195657004](https://gitee.com/feng2828/image/raw/master/img/20210723142235.png)

**③合并两个分支**

![image-20210719200053899](https://gitee.com/feng2828/image/raw/master/img/20210723142239.png)

提示 CONFLICT ,产生冲突

冲突产生的表现 ：后面的状态为 ==MERGING==，如上图

`冲突产生的原因`：

合并分支时，两个分支在同一个文件的同一个位置有两套完全不同的修改。Git 无法替我们决定使用哪一个。 必须人为决定新代码内

容。

查看状态(检测到有文件有两处修改)

![image-20210719200228178](https://gitee.com/feng2828/image/raw/master/img/20210723142244.png)

==相当于==：**用 hot-fix 覆盖了master，实际是指针指向了 hot-fix 分支，相比原来的 master 有两处修改**

### 4.3.7、解决冲突

**1)编辑有冲突的文件，删除特殊符号，决定要使用的内容**

特殊符号:  ==<<<<<< HEAD==    当前分支的代码   `=======`    合并过来的代码    ==>>>>>> hot-fix==

![image-20210719200705511](https://gitee.com/feng2828/image/raw/master/img/20210723142247.png)

将这些修改为下图，保存

![image-20210719201053339](https://gitee.com/feng2828/image/raw/master/img/20210723142250.png)



**2）添加到暂存区**

![image-20210719201447618](https://gitee.com/feng2828/image/raw/master/img/20210723142254.png)

**3）执行提交（注意：此时使用 git commit 命令时不能带文件名）**

![image-20210719202026751](https://gitee.com/feng2828/image/raw/master/img/20210723142257.png)



**这个时候master分支会合并，但是在hot-fix 分支还是原来的，想要在master基础上再改，就要创建新分支**

![image-20210719202314696](https://gitee.com/feng2828/image/raw/master/img/20210723142301.png)



## 4.4、创建分支和切换分支图解

![image-20210719202552291](https://gitee.com/feng2828/image/raw/master/img/20210723142306.png)

​		master、hot-fix 其实都是指向具体版本记录的指针。当前所在的分支，其实是由HEAD决定的。所以创建分支的本质就是多创建一个指针。

**HEAD**如果指向master,那么我们现在就在master分支上。

**HEAD**如果执行hot-fix，那么我们现在就在hot-fix分支上。

所以切换分支的本质就是移动HEAD指针。

`就是`  :  HEAD => master => 具体的版本



# 五、Git 团队协作机制

## 5.1、团队内协作

![image-20210719204628833](https://gitee.com/feng2828/image/raw/master/img/20210723142311.png)

**这里令狐冲 push(推送) 的时候需要岳不群的权限，让令狐冲是团队的一员**

==clone 是从无到有的 ，pull (拉取) 是在已有的本地库上拉取远程仓库进行更新==

## 5.2、跨团队协作

![image-20210719205727035](https://gitee.com/feng2828/image/raw/master/img/20210723142320.png)

岳不群在远程库里 fork 给东方不败，东方不败 clone 下来修改再 push 上去，然后 发送 Pull Request 给 岳不群，岳不群审核后 merge 合并到自己的远程库，最后pull 下来更新



# 六、GitHub操作

`官网`：https://github.com/

Ps:全球最大同性交友网站，技术宅男的天堂，新世界的大门，你还在等什么?

|   账号    |   姓名   |        邮箱         |
| :-------: | :------: | :-----------------: |
|  Feng232  |  岳不群  |  308266103@qq.com   |
| Feng2322  |  令狐冲  | feng1230719@163.com |
| Feng23222 | 东方不败 | feng3212021@163.com |



## 6.1、创建远程仓库

![image-20210719212936855](https://gitee.com/feng2828/image/raw/master/img/20210723142324.png)



![image-20210719214903603](https://gitee.com/feng2828/image/raw/master/img/20210723142328.png)

创建好后的远程库 Http 链接为：https://github.com/Feng232/git-demo.git

![image-20210720110035797](https://gitee.com/feng2828/image/raw/master/img/20210723142339.png)

## 6.2、远程仓库操作

![image-20210719215148298](https://gitee.com/feng2828/image/raw/master/img/20210723142411.png)



### 6.2.1、创建远程仓库别名

**1)基本语法**

```
git remote -v    查看当前所有远程地址别名

git remote add     别名    远程库地址

```

**2)实例操作**

![image-20210720105949972](https://gitee.com/feng2828/image/raw/master/img/20210723142417.png)



### 6.2.2、推送本地分支到远程仓库

**1）基本语法**

````
git push   远程库别名   分支
````

**2）实操**

![image-20210720111731717](https://gitee.com/feng2828/image/raw/master/img/20210723142420.png)

**刷新远程仓库，发现推送成功**

![image-20210720111821549](https://gitee.com/feng2828/image/raw/master/img/20210723142424.png)



### 6.2.3、从远程库拉取Pull 到本地库

修改了远程库的代码

![image-20210720112757130](https://gitee.com/feng2828/image/raw/master/img/20210723142427.png)

  **1）语法**

```
git  pull 远程库别名 分支名
```

**2）示例**

![image-20210720113026435](https://gitee.com/feng2828/image/raw/master/img/20210723142431.png)

结果

![image-20210720113124831](https://gitee.com/feng2828/image/raw/master/img/20210723142434.png)



### 6.2.4、克隆远程仓库到本地

**1）语法**

```
git clone 远程库地址
```

**2）示例**

我们新建了一个文件夹

![image-20210720115931905](Git.assets/image-20210720115931905.png)

在这里面启动 Git Bush ,输入命令

![image-20210720120004211](https://gitee.com/feng2828/image/raw/master/img/20210723142437.png)

**3）结果**

克隆成功

![image-20210720120042588](https://gitee.com/feng2828/image/raw/master/img/20210723142441.png)

`小结`: clone会做三件事：

1.  拉取代码 

2. 初始化本地库   

3. 创建别名

   ![image-20210720120413979](https://gitee.com/feng2828/image/raw/master/img/20210723142444.png)



### 6.2.5、邀请加入团队

**1）选择邀请合作者**

![image-20210720125707078](https://gitee.com/feng2828/image/raw/master/img/20210723142447.png)

**邀请好后复制邀请函**

![image-20210720125912086](https://gitee.com/feng2828/image/raw/master/img/20210723142451.png)

邀请函：https://github.com/Feng232/git-demo/invitations

**2）受邀请的令狐冲在自己的GitHub里粘贴链接接受邀请**

![image-20210720130155237](https://gitee.com/feng2828/image/raw/master/img/20210723142454.png)

**这个时候推送就不会收到限制**

![image-20210720130506088](https://gitee.com/feng2828/image/raw/master/img/20210723142501.png)



## 6.3、跨团队协作

**1）在东方不败的 Github 里输入岳不群的远程库链接 ，然后fork**

![image-20210720143027599](https://gitee.com/feng2828/image/raw/master/img/20210723142506.png)

![image-20210720143155404](https://gitee.com/feng2828/image/raw/master/img/20210723142534.png)

**2）这个时候东方不败就可以自己 clone 下来改了**

![image-20210720143358684](https://gitee.com/feng2828/image/raw/master/img/20210723142538.png)

**3）东方不败发送 Pull Request**

![image-20210720143610094](https://gitee.com/feng2828/image/raw/master/img/20210723142542.png)

![image-20210720143700376](https://gitee.com/feng2828/image/raw/master/img/20210723142545.png)

![image-20210720143819743](https://gitee.com/feng2828/image/raw/master/img/20210723142548.png)

**4）这个时候岳不群接受 Pull Request**

![image-20210720143952307](https://gitee.com/feng2828/image/raw/master/img/20210723142552.png)

![image-20210720144032357](https://gitee.com/feng2828/image/raw/master/img/20210723142556.png)

**审核后同意，merge 合并**

![image-20210720144402490](https://gitee.com/feng2828/image/raw/master/img/20210723142559.png)

![image-20210720144430413](https://gitee.com/feng2828/image/raw/master/img/20210723142603.png)



**5）这个时候就跨团队合作完成了**

![image-20210720144553838](https://gitee.com/feng2828/image/raw/master/img/20210723142606.png)



## 6.4、SSH免密登录

**我们可以看到远程仓库中还有一个SSH的地址，因此我们也可以使用SSH进行访问。**

![image-20210720144813821](https://gitee.com/feng2828/image/raw/master/img/20210723142610.png)



**1）在自己帐户的文件下右键 Git Bash**

![image-20210720162651876](https://gitee.com/feng2828/image/raw/master/img/20210723142615.png)

**2)执行命令**

```
ssh-keygen -t  rsa(加密协议) -C GitHub帐号
```

![image-20210720163837313](https://gitee.com/feng2828/image/raw/master/img/20210723142619.png)

**3)发现生成了一个 `.ssh` 文件**

![image-20210720163903814](https://gitee.com/feng2828/image/raw/master/img/20210723142622.png)

**4）把这个文件里的的公钥复制，还有一个是私钥**

![image-20210720164951036](https://gitee.com/feng2828/image/raw/master/img/20210723142625.png)

或者命令复制

![image-20210720165028680](https://gitee.com/feng2828/image/raw/master/img/20210723142629.png)



**5）在GitHub账户的设置里进行绑定**

![image-20210720164522562](https://gitee.com/feng2828/image/raw/master/img/20210723142632.png)

![image-20210720164554762](https://gitee.com/feng2828/image/raw/master/img/20210723142637.png)

![image-20210720164716716](https://gitee.com/feng2828/image/raw/master/img/20210723142726.png)

**添加名字和公匙**

![image-20210720165110740](https://gitee.com/feng2828/image/raw/master/img/20210723142731.png)



**6）添加成功**

![image-20210720165254019](https://gitee.com/feng2828/image/raw/master/img/20210723142734.png)

这个时候已经有了 SSH

![image-20210720165600079](https://gitee.com/feng2828/image/raw/master/img/20210723142738.png)

**7)测试**

在远程库里有东方不败

![image-20210720165640231](https://gitee.com/feng2828/image/raw/master/img/20210723142742.png)

在本地库没有

![image-20210720165705678](https://gitee.com/feng2828/image/raw/master/img/20210723142745.png)



**通过SSH链接进行拉取更新**

![image-20210720165943642](https://gitee.com/feng2828/image/raw/master/img/20210723142750.png)

再次查看文件

![image-20210720170114344](https://gitee.com/feng2828/image/raw/master/img/20210723142754.png)

**成功！**

**测试一下push**

修改文件

![image-20210720170323142](https://gitee.com/feng2828/image/raw/master/img/20210723142758.png)

提交到本地库

![image-20210720170450526](https://gitee.com/feng2828/image/raw/master/img/20210723142806.png)

推送到远程库用 ssh 密匙

![image-20210720170615826](https://gitee.com/feng2828/image/raw/master/img/20210723142809.png)

**网页端检查 ，成功！**

![image-20210720170701008](https://gitee.com/feng2828/image/raw/master/img/20210723142812.png)





# 七、IDEA集成Git

## 7.1、配置Git忽略文件

`就是忽略一些多余的文件`

**1）Eclipse 特定文件**

![image-20210720172846417](https://gitee.com/feng2828/image/raw/master/img/20210723142816.png)

**2）IDEA 特定文件**

![image-20210720173113121](https://gitee.com/feng2828/image/raw/master/img/20210723142821.png)



**3）Maven工程的 target 目录**

![image-20210720173213082](https://gitee.com/feng2828/image/raw/master/img/20210723142824.png)

**问题1:为什么要忽略他们?**

答 : 与项目的实际功能无关，不参与服务器上部署运行。把它们忽略掉能够屏蔽IDE工具之
间的差异。

**问题2:怎么忽略?** 
1**)创建忽略规则文件 `xxxx.ignore` (前缀名随便起，建议是`git.ignore`)**

这个文件的存放位置原则上在哪里都可以，为了便于让~/.gitconfig文件引用，建议也放在用户家目录下

**git.ignore文件模版内容如下:**

```properties
#Compiled class file
*.class

#Log file
*.log

# BlueJ files
*.ctxt

# Mobile Tools for Java (J2ME)←
.mtj.tmp/

# Package Files #
*.jar
*.war
*.nar
*.ear
*.zip
*.tar.gz
*.rar


#  virtual  machine   crash   logs, see http://www.java.com/en/download/he1p/error_ hotspot.xml
hs_err_pid*

.classpath
.project
.settings
target
.idea
*.iml
```

```properties
## .gitignore for Grails 1.2 and 1.3

### maven  ###
target/
*.releaseBackup

### gradle ###
gradle
gradlew
gradlew.bat

## hack for graddle wrapper
!wrapper/*.jar
!**/wrapper/*.jar

### java ###
classes/
*.class
*.war
*.ear
 
###  ide ### 
/.classpath
/.launch
/.project
/.settings
/*.launch
/*.tmproj
/ivy*
/eclipse
.apt_generated
.factorypath
.springBeans

### idea ###
.idea
*.iml
*.ipr
*.iws

### eclipse ###
*.pydevproject
.project
.cproject
.metadata
.buildpath
bin/**
tmp/**
tmp/**/*
*.tmp
*.bak
*.swp
*~.nib
local.properties
.classpath
.settings/
.loadpath
*.launch
.externalToolBuilders/

### NetBeans ###
nbproject/private/
build/
nbbuild/
#dist/
nbdist/
.nb-gradle/
ticket-static/.sass-cache/
 
### hsql 数据库文件 ###
/prodDb.*
*Db.properties
*Db.script
 
### 日志 ###
/stacktrace.log
/test/reports
/logs
*.log
*.log.*
applogo/
 
### 较旧的插件安装位置 ###
/plugins
/web-app/plugins
/web-app/WEB-INF/classes
 
### 临时文件 ###
target/
out/
build/

### windows ###
Thumbs.db
ehthumbs.db
Desktop.ini
 
### linux ###
!.gitignore
*~
 
## mac ###
.DS_Store
.AppleDouble
.LSOverride
Icon
.Spotlight-V100
.Trashes

### npm yarn ###
node_modules/
/dist/
package-lock.json
npm-debug.log*
yarn-debug.log*
yarn-error.log*
yarn-lock.json
 
# Thumbnails
._*

### 其他 ###
*.iws

```

**2)在. gitconfig文件中引用忽略配置文件(此文件在Windows的家目录中)**

```
[filter "lfs"]
	clean = git-lfs clean -- %f
	smudge = git-lfs smudge -- %f
	process = git-lfs filter-process
	required = true
[user]
	name = Feng
	email = 308266103@qq.com
[core]
	excludestile = C:/Users/Hey!boy/git.ignore
注意:这里要使用“正斜线(/)” ,不要使用“反斜线"(\)"
```

==①在用户目录下创建 git.ignore,并添加上述内容==

![image-20210720174732417](https://gitee.com/feng2828/image/raw/master/img/20210723142833.png)

==②引用配置文件==

![image-20210720180315457](https://gitee.com/feng2828/image/raw/master/img/20210723142841.png)



`插件方法（推荐使用这个）`

==注意==：先写忽略文件再建库，如果忽略不了

[文件夹]  git rm -r --cached 文件夹

[文件]   git rm --cached 文件

![image-20210720191744379](https://gitee.com/feng2828/image/raw/master/img/20210723142845.png)

![image-20210720191827063](https://gitee.com/feng2828/image/raw/master/img/20210723142848.png)

![image-20210720193456100](https://gitee.com/feng2828/image/raw/master/img/20210723142852.png)

忽略后的文件会变灰色

## 7.2、定位Git程序

**这是 git.exe  的安装位置**

![image-20210720180722429](https://gitee.com/feng2828/image/raw/master/img/20210723142859.png)

**在IDEA设置里**

![image-20210720181646155](https://gitee.com/feng2828/image/raw/master/img/20210723142902.png)



## 7.3、初始化IDEA本地库

**创建本地库**

![image-20210720181931974](https://gitee.com/feng2828/image/raw/master/img/20210723142906.png)

**这里创建本地库后，选择项目，确定，发现IDEA变化，以及在文件目录下有了 .git 文件**

![image-20210720182425608](https://gitee.com/feng2828/image/raw/master/img/20210723142910.png)

![image-20210720182433521](https://gitee.com/feng2828/image/raw/master/img/20210723142913.png)

## 7.4.添加到暂存区

![image-20210720182612375](https://gitee.com/feng2828/image/raw/master/img/20210723142918.png)

## 7.5、提交本地库

![image-20210720183245356](https://gitee.com/feng2828/image/raw/master/img/20210723142921.png)



![image-20210720183643810](https://gitee.com/feng2828/image/raw/master/img/20210723142924.png)



## 7.6、切换版本

这里我们有三个版本

![image-20210720194027253](https://gitee.com/feng2828/image/raw/master/img/20210723142928.png)

**如果我们需要切换版本点击版本右键选择**

![image-20210720194212291](https://gitee.com/feng2828/image/raw/master/img/20210723142931.png)

**结果：头指针发生变化，代码也对应发生变化**

![image-20210720194328117](https://gitee.com/feng2828/image/raw/master/img/20210723142934.png)



## 7.7、创建分支&切换分支

> 创建分支

**项目右键**

![image-20210720195815503](https://gitee.com/feng2828/image/raw/master/img/20210723142942.png)

![image-20210720195854739](https://gitee.com/feng2828/image/raw/master/img/20210723142946.png)

![image-20210720195950752](https://gitee.com/feng2828/image/raw/master/img/20210723142949.png)





​	**或者IDEA右下角 master**

![image-20210720200115348](https://gitee.com/feng2828/image/raw/master/img/20210723142954.png)

创建好后

![image-20210720200315884](https://gitee.com/feng2828/image/raw/master/img/20210723142957.png)

> 切换分支

![image-20210720200452121](https://gitee.com/feng2828/image/raw/master/img/20210723143001.png)

结果

![image-20210720200530880](https://gitee.com/feng2828/image/raw/master/img/20210723143005.png)



## 7.8.合并分支

`正常合并，只修改了 hot-fix`

**在IDEA窗口的右下角，将hot-fix分支合并到当前master分支。**

![image-20210720201359868](https://gitee.com/feng2828/image/raw/master/img/20210723143010.png)



**如果代码没有冲突，分支直接合并成功，分支合并成功以后，代码自动提交，无需手动提交本地库。**

![image-20210720201420493](https://gitee.com/feng2828/image/raw/master/img/20210723143016.png)



## 7.9、解决冲突

 `有冲突时，两个分支都变了`

![image-20210720203532982](https://gitee.com/feng2828/image/raw/master/img/20210723143021.png)

这个时候再合并分支弹出

![image-20210720203704875](https://gitee.com/feng2828/image/raw/master/img/20210723143024.png)

**点击 Merge，合并**

![image-20210720204223219](https://gitee.com/feng2828/image/raw/master/img/20210723143029.png)

**完成后**

![image-20210720204616404](https://gitee.com/feng2828/image/raw/master/img/20210723143039.png)



# 八、IDEA集成GitHub

## 8.1、设置 GitHub账号

**1）设置添加**

![image-20210720211544709](https://gitee.com/feng2828/image/raw/master/img/20210723143102.png)

**2）认证**

![image-20210720211506439](https://gitee.com/feng2828/image/raw/master/img/20210723143106.png)

![image-20210720211555554](https://gitee.com/feng2828/image/raw/master/img/20210723143111.png)



## 8.2、使用令牌登录

**1.个人设置**

![image-20210720212540412](https://gitee.com/feng2828/image/raw/master/img/20210723143115.png)



![image-20210720212618280](https://gitee.com/feng2828/image/raw/master/img/20210723143118.png)

**2.选择最后一个**

![image-20210720212703757](https://gitee.com/feng2828/image/raw/master/img/20210723143126.png)

**3.选择权限，全选**

![image-20210720212836696](https://gitee.com/feng2828/image/raw/master/img/20210723143130.png)

**4、生成令牌**

![image-20210720212940430](https://gitee.com/feng2828/image/raw/master/img/20210723143133.png)

为：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

**5、IDEA添加帐户**

![image-20210720213047735](https://gitee.com/feng2828/image/raw/master/img/20210723143137.png)



## 8.3、IDEA项目分享到GitHub

![image-20210720213607769](https://gitee.com/feng2828/image/raw/master/img/20210723143144.png)

![image-20210720213826252](https://gitee.com/feng2828/image/raw/master/img/20210723143149.png)



在网页端查看，发现成功

![image-20210720214003203](https://gitee.com/feng2828/image/raw/master/img/20210723143155.png)



## 8.4、 Push 推送到远程库（养成习惯，先pull再push）

> 第一种

**右键点击项目，可以将当前分支的内容push到GitHub的远程仓库中。**

![image-20210720214640643](https://gitee.com/feng2828/image/raw/master/img/20210723143159.png)

> 第二种

![image-20210720214945375](https://gitee.com/feng2828/image/raw/master/img/20210723143205.png)



`这里用SSH来push`

![image-20210720215222504](https://gitee.com/feng2828/image/raw/master/img/20210723143208.png)

**这里用SSH的链接**

![image-20210720215310132](https://gitee.com/feng2828/image/raw/master/img/20210723143211.png)

![image-20210720215441664](https://gitee.com/feng2828/image/raw/master/img/20210723143217.png)

![image-20210720215455521](https://gitee.com/feng2828/image/raw/master/img/20210723143221.png)

**推送成功**

![image-20210720215600463](https://gitee.com/feng2828/image/raw/master/img/20210723143225.png)



`注意`

push 是将本地库代码推送到远程库，如果本地库代码跟远程库代码版本不一致，push的操作是会被拒绝的。

也就是说**，要想push成功，一定要保证本地库的版本要比远程库的版本高 !** 

因此一个成熟的程序员在动手改本地代码之前，一定会先检查下远程库跟本地代码的区别 ! 

如果本地的代码版本已经落后，切记要先pull拉取一下远程库的代码，将本地代码更新到最新以后，然后再修改，提交，推送!



## 8.5、 Pull拉取远程库到本地库

`注意`: 

pull 是拉取远端仓库代码到本地，如果远程库代码和本地库代码不一致，会自动合并，

如果自动合并失败，还会涉及到手动解决冲突的问题。



**右键点击项目，可以将远程仓库的内容pull到本地仓库。**

![image-20210720215801892](https://gitee.com/feng2828/image/raw/master/img/20210723143230.png)



远程库代码进行修改

![image-20210720215957243](https://gitee.com/feng2828/image/raw/master/img/20210723143236.png)

**进行Pull操作**

![image-20210720220837678](https://gitee.com/feng2828/image/raw/master/img/20210723143241.png)

**成功！**

![image-20210720220922687](https://gitee.com/feng2828/image/raw/master/img/20210723143244.png)



## 8.6、Clone 克隆远程库到本地

`没有项目时`

![image-20210720221435810](https://gitee.com/feng2828/image/raw/master/img/20210723143250.png)

![image-20210720221812615](https://gitee.com/feng2828/image/raw/master/img/20210723143256.png)



`有项目在IDEA时`

![image-20210720222048653](https://gitee.com/feng2828/image/raw/master/img/20210723143326.png)



## 8.7、附加配置

#### 修改idea终端为Git bash

修改控制台shell路径：setting -> Tools -> Terminal -> Shell path，修改为GitBash.exe路径。

![image-20210720223157852](https://gitee.com/feng2828/image/raw/master/img/20210723143339.png)

# 九、国内代码托管中心-码云

## 9.1、简介

**网址**：

众所周知,GitHub服务器在国外，使用GitHub作为项目托管网站，如果网速不好的话，严重影响使用体验，甚至会出现登录不上的情

况。针对这个情况，码云是开源中国推出的基于Git的代码托管服务中心，大家也可以使用国内的项目托管网站---------`码云`



## 9.2、码云创建远程库

**1)点击首页右上角的加号，选择下面的新建仓库**

![image-20210720223810310](https://gitee.com/feng2828/image/raw/master/img/20210723143415.png)



![image-20210720224031461](https://gitee.com/feng2828/image/raw/master/img/20210723143418.png)

**2)创建完成**

![image-20210720224229977](https://gitee.com/feng2828/image/raw/master/img/20210723143423.png)



## 9.3、IDEA集成 Gitee

### 9.3.1、IDEA安装码云插件

![image-20210721100141321](https://gitee.com/feng2828/image/raw/master/img/20210723143427.png)

### 9.3.2、登录码云账号

![image-20210721100723499](https://gitee.com/feng2828/image/raw/master/img/20210723143433.png)



### 9.3.3、分享项目到码云

![image-20210721101440973](https://gitee.com/feng2828/image/raw/master/img/20210723143437.png)



**其他操作基本和GitHub 一样**



## 9.4、码云复制 GitHub项目

**码云提供了直接复制GitHub项目的功能，方便我们做项目的迁移和下载。**

具体操作如下:



在新建仓库的下面有一个导入已有仓库

![image-20210721120538799](https://gitee.com/feng2828/image/raw/master/img/20210723143448.png)

在这里输入GitHub 项目链接或者登录账号

![image-20210721120735217](https://gitee.com/feng2828/image/raw/master/img/20210723143509.png)

**创建好后，按这里可以刷新GitHub更新的内容**

![image-20210721121248307](https://gitee.com/feng2828/image/raw/master/img/20210723143514.png)



# 十、自建代码托管平台------GitLab

## 10.1、GitLab简介

​		GitLab 是由 GitIabInc.开发，使用MIT许可证的基于网络的Git仓库管理工具，且具有 wiki 和 issue 跟踪功能。使用Git作为代码管

理工具，并在此基础上搭建起来的web服务。

​		GitLab由乌克兰程序员 DmitriyZaporozhets 和 ValerySizov 开发，它使用Ruby语言写成。后来,一些部分用Go语言重写。截止

2018年5月，该公司约有290名团队成员，以及2000多名开源贡献者。

​		GitLab 被IBM, Sony, JilichResearchCenter， NASA， Alibaba,Invincea, O'ReillyMedia, Leibniz- Rechenzentrum(LRZ)，CERN， SpaceX 等组织使用。θ

## 10.2、官网地址

**地址：**https://about.gitlab.com/

**安装说明**：https://about.gitlab.com/install/

![image-20210812104547478](https://gitee.com/feng2828/image/raw/master/img/20210812104547.png)

## 10.3、安装

### 10.3.1、服务器准备

准备一个系统为`CentOS7`以上版本的服务器，要求内存 4G，磁盘 50G。

关闭防火墙，并且配置好主机名和P，保证服务器可以上网。

此时我们的虚拟机IP地址是 192.168.174.128 

![image-20210812110707017](https://gitee.com/feng2828/image/raw/master/img/20210812110707.png)

此教程使用虚拟机:主机名:  gitlab-server     修改IP 地址为: 192.168.174.200

![image-20210721124327973](https://gitee.com/feng2828/image/raw/master/img/20210723143521.png)

![image-20210721124353005](https://gitee.com/feng2828/image/raw/master/img/20210723143529.png)

![image-20210812111842929](https://gitee.com/feng2828/image/raw/master/img/20210812111843.png)

![image-20210721124440519](https://gitee.com/feng2828/image/raw/master/img/20210723143533.png)

![image-20210721124459984](https://gitee.com/feng2828/image/raw/master/img/20210723143536.png)

**改好后重启**

![image-20210721124541280](https://gitee.com/feng2828/image/raw/master/img/20210723143542.png)



在Windows的Hosts里改映射

![image-20210721124741228](https://gitee.com/feng2828/image/raw/master/img/20210723143546.png)

![image-20210721124800325](https://gitee.com/feng2828/image/raw/master/img/20210723143549.png)

### 10.3.2、安装包准备

1. Yum 在线安装 gitlab- ce 时，需要下载几百 M 的安装文件，非常耗时，所以最好提前把所需 RPM 包下载到本地，然后使用离线 rpm 的方式安装。
2. 下载地址：https://packages.gitlab.com/gitlab/gitlab-ce/packages/el/7/gitlab-ce-13.10.2-ce.0.el7.x86_64.rpm
3. 注：资料里提供了此 rpm 包，直接将此包上传到服务器`/opt/module `目录下即可。这个目录是创建的，因为在opt目录下，不用了可以直接删除文件夹就可以删除程序

![image-20210812120026979](https://gitee.com/feng2828/image/raw/master/img/20210812120027.png)

### 10.3.3、编写安装脚本

安装 gitlab 步骤比较繁琐，因此我们可以参考官网编写 gitlab 的安装脚本

https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh

在这个 module 目录下

```bash
[root@gitlab-server module]# vim gitlab-install.sh
sudo rpm -ivh /opt/module/gitlab-ce-13.10.2-ce.0.el7.x86_64.rpm

sudo yum install -y curl policycoreutils-python openssh-server cronie

sudo lokkit -s http -s ssh

sudo yum install -y postfix

sudo service postfix start

sudo chkconfig postfix on

curl https://packages.gitlab.com/install/repositories/gitlab/gitlabce/script.rpm.sh | sudo bash

sudo EXTERNAL_URL="http://gitlab.example.com" yum -y install gitlabce

```

wq保存

![image-20210812120718938](https://gitee.com/feng2828/image/raw/master/img/20210812120719.png)

给脚本增加执行权限

```bash
[root@gitlab-server module]# chmod +x gitlab-install.sh
[root@gitlab-server module]# ll
总用量 403104
-rw-r--r--. 1 root root 412774002 4 月 7 15:47 gitlab-ce-13.10.2-
ce.0.el7.x86_64.rpm
-rwxr-xr-x. 1 root root 416 4 月 7 15:49 gitlab-install.sh

```

![image-20210812120749894](https://gitee.com/feng2828/image/raw/master/img/20210812120749.png)

然后执行该脚本，开始安装 gitlab-ce。注意一定要保证服务器可以上网。

```bash
[root@gitlab-server module]# ./gitlab-install.sh
警告： /opt/module/gitlab-ce-13.10.2-ce.0.el7.x86_64.rpm: 头 V4
RSA/SHA1 Signature, 密钥 ID f27eab47: NOKEY
准备中... #################################
[100%]
正在升级/安装...
1:gitlab-ce-13.10.2-ce.0.el7
################################# [100%]
。 。 。 。 。 。
```

![image-20210812144324760](https://gitee.com/feng2828/image/raw/master/img/20210812144324.png)

### 10.3.4、初始化GitLab 服务

行以下命令初始化 GitLab 服务，过程大概需要几分钟，耐心等待…

```bash
[root@gitlab-server module]# gitlab-ctl reconfigure
。 。 。 。 。 。
Running handlers:
Running handlers complete
Chef Client finished, 425/608 resources updated in 03 minutes 08
seconds
gitlab Reconfigured!
```

![image-20210812145001008](https://gitee.com/feng2828/image/raw/master/img/20210812145001.png)

### 10.3.5、启动GitLab服务

```bash
[root@gitlab-server module]# gitlab-ctl start
ok: run: alertmanager: (pid 6812) 134s
ok: run: gitaly: (pid 6740) 135s
ok: run: gitlab-monitor: (pid 6765) 135s
ok: run: gitlab-workhorse: (pid 6722) 136s
ok: run: logrotate: (pid 5994) 197s
ok: run: nginx: (pid 5930) 203s
ok: run: node-exporter: (pid 6234) 185s
ok: run: postgres-exporter: (pid 6834) 133s
ok: run: postgresql: (pid 5456) 257s
ok: run: prometheus: (pid 6777) 134s
ok: run: redis: (pid 5327) 263s
ok: run: redis-exporter: (pid 6391) 173s
ok: run: sidekiq: (pid 5797) 215s
ok: run: unicorn: (pid 5728) 221s
```

停止就是 `gitlab-ctl stop`

还有 ：`systemctl stop gitlab-runsvdir`

### 10.3.6、使用浏览器访问GitLab

### 登录GitLab

**GitLab端口默认是80，所以云服务器，或者本机下面这种设置，直接访问IP就行** http://119.23.51.34

使用主机名或者 IP 地址即可访问 GitLab 服务。可配一下 windows 的 hosts 文件（C:\Windows\System32\drivers\etc）。

![image-20210812114945273](https://gitee.com/feng2828/image/raw/master/img/20210812114945.png)

### 10.3.7、GitLab创建远程库

![image-20210812145152910](https://gitee.com/feng2828/image/raw/master/img/20210812145153.png)

首次登陆之前，需要修改下 GitLab 提供的 root 账户的密码，要求 8 位以上，包含大小写子母和特殊符号。

然后使用修改后的密码登录 GitLab。

这里设置的是 LIU20010124ljf!

![image-20210812115023972](https://gitee.com/feng2828/image/raw/master/img/20210812115024.png)

![image-20210812145537551](https://gitee.com/feng2828/image/raw/master/img/20210812145537.png)

### 创建远程库

![image-20210812145757172](https://gitee.com/feng2828/image/raw/master/img/20210812145757.png)

![image-20210812150307189](https://gitee.com/feng2828/image/raw/master/img/20210812150307.png)

![image-20210812115118532](https://gitee.com/feng2828/image/raw/master/img/20210812115118.png)

### 10.3.8、IDEA集成GitLab

###  ![image-20210812151016078](https://gitee.com/feng2828/image/raw/master/img/20210812151016.png)

![image-20210812152312991](https://gitee.com/feng2828/image/raw/master/img/20210812152313.png)



![image-20210812152641146](https://gitee.com/feng2828/image/raw/master/img/20210812152641.png)

![image-20210812152842085](https://gitee.com/feng2828/image/raw/master/img/20210812152842.png)

![image-20210812153004696](https://gitee.com/feng2828/image/raw/master/img/20210812153004.png)

![image-20210812153027076](https://gitee.com/feng2828/image/raw/master/img/20210812153027.png)









