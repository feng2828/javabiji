# 1.初始MySQL

![输入图片说明](https://images.gitee.com/uploads/images/2021/0722/213938_2d605c6d_9028246.png "image-20210507105855973.png")

## 1.1 为什么学习数据库

![image-20210507110433841](https://gitee.com/feng2828/image/raw/master/img/20210723144405.png)

## 1.2 什么是数据库

![image-20210507110931835](https://gitee.com/feng2828/image/raw/master/img/20210723144620.png)

## 1.3 数据库分类

关系型数据库：（SQL）

- MySQL ，Oracle, Sql Server ,DB2
- 通过表和表之间，行和列之间的关系进行数据的存储

非关系型数据库：(NOSQL) not Only SQL

- Redis , MondDB
- 对象存储，通过对象自身的属性来决定

**DBMS(数据库管理系统)**

![image-20210507113143780](https://gitee.com/feng2828/image/raw/master/img/20210723144621.png)



![image-20210507113129387](https://gitee.com/feng2828/image/raw/master/img/20210723144622.png)



## 1.4Mysql简介

![image-20210507114748218](https://gitee.com/feng2828/image/raw/master/img/20210723144623.png)

 ![image-20210507114809207](https://gitee.com/feng2828/image/raw/master/img/20210723144624.png)

## 1.5 安装MySQL

压缩包版本

1.解压

2.把这个包配置到电脑环境目录path下，就是添加MySQL的bin的目录

3.新建MySQL 配置文件

![image-20210507115603318](https://gitee.com/feng2828/image/raw/master/img/20210723144625.png)

![image-20210507115845724](https://gitee.com/feng2828/image/raw/master/img/20210723144626.png)

4.启动**管理员**模式的CMD，运行所有的命令

![image-20210507120135911](https://gitee.com/feng2828/image/raw/master/img/20210723144627.png)

5.安装mysql服务

6.初始化数据库文件

7.启动mysql服务，进去修改密码

![image-20210507122052575](https://gitee.com/feng2828/image/raw/master/img/20210723144628.png)

![image-20210507123908191](https://gitee.com/feng2828/image/raw/master/img/20210723144629.png)

> sc delete mysql ,清空服务

## 1.6 安装SQLyog

![image-20210507170702751](https://gitee.com/feng2828/image/raw/master/img/20210723144630.png)

```
#修改加密规则 ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '密码';
#刷新权限 FLUSH PRIVILEGES;
```

查询一下数据存在哪里 ：show global variables like "%datadir%"; 打开配置文件搜索：datadir

## 1.7 创建数据库 

![image-20210507180830780](https://gitee.com/feng2828/image/raw/master/img/20210723144631.png)

创表

![image-20210507181608639](https://gitee.com/feng2828/image/raw/master/img/20210723144632.png)

## 1.8 连接数据库

mysql -uroot-p

C:\Program Files\MySQL\MySQL Shell 8.0\bin\

**修改MySQL密码**

MySQL 5.7 的版本，因为在user表中没有password字段，一直使用下边的方式来修改root密码



```
use mysql; 
update user set authentication_string = password(“root”) where user = “root”;
```


现在要用MySQL8.0.11版本，装好MySQL后用上边方法修改密码，一直报错。后来去掉password()函数后，没有报错，但是输入密码时不对。

查阅后才知道在mysql 5.7.9以后废弃了password字段和password()函数；authentication_string:字段表示用户密码，而authentication_string字段下只能是mysql加密后的41位字符串密码。所以需要用一下方式来修改root密码：


ALTER user 'root'@'localhost' IDENTIFIED BY 'newpassword';
MySql 从8.0开始修改密码有了变化，在user表加了字段authentication_string，修改密码前先检查authentication_string是否为空

1、如果不为空

```sql
use mysql; 
update user set authentication_string='' where user='root';--将字段置为空
ALTER user 'root'@'localhost' IDENTIFIED BY 'root';--修改密码为root
```


2、如果为空，直接修改

```sql
ALTER user 'root'@'localhost' IDENTIFIED BY 'root';--修改密码为root
```

如果出现如下错误

```sql
ERROR 1290 (HY000): The MySQL server is running with the --skip-grant-tables option so it cannot execute this statement
mysql> GRANT ALL PRIVILEGES ON *.* TO IDENTIFIED BY '123' WITH GRANT OPTION;
```

需要执行;

```sql
flush privileges
```



然后再执行

```sql
ALTER user 'root'@'localhost' IDENTIFIED BY 'root';--修改密码为root
```



**所有的语句都使用分号结尾**

```sql
show databases; ---查看所有的数据库
```

**常用命令 **

![image-20210507191646340](https://gitee.com/feng2828/image/raw/master/img/20210723144633.png)

![image-20210507192059394](https://gitee.com/feng2828/image/raw/master/img/20210723144634.png)

# 2.操作数据库

![image-20210507192521926](https://gitee.com/feng2828/image/raw/master/img/20210723144635.png)

mysql关键字不区分大小写

## 2.1 操作

![image-20210507194013641](https://gitee.com/feng2828/image/raw/master/img/20210723144636.png)

## 2.2 数据库的列的数据类型

![image-20210507194720321](https://gitee.com/feng2828/image/raw/master/img/20210723144637.png)

![image-20210507195019464](https://gitee.com/feng2828/image/raw/master/img/20210723144638.png)

![image-20210507195256831](https://gitee.com/feng2828/image/raw/master/img/20210723144639.png)

![image-20210507195337665](https://gitee.com/feng2828/image/raw/master/img/20210723144640.png)

## 2.3 字段属性（重点）

![image-20210507200053076](https://gitee.com/feng2828/image/raw/master/img/20210723144641.png)

![image-20210507200149029](https://gitee.com/feng2828/image/raw/master/img/20210723144642.png)

拓展：

![image-20210507201742363](https://gitee.com/feng2828/image/raw/master/img/20210723144643.png)

## 2.4 创建数据库表（重点）

![image-20210507204132977](https://gitee.com/feng2828/image/raw/master/img/20210723144644.png)

![image-20210507204452318](https://gitee.com/feng2828/image/raw/master/img/20210723144645.png)

常用命令

![image-20210507204919673](https://gitee.com/feng2828/image/raw/master/img/20210723144646.png)

## 2.5 数据表的类型

```sql
--关于数据库引擎
/*
INNODB 默认使用
MYISAM 早些年使用

*/
```

![image-20210507205737857](https://gitee.com/feng2828/image/raw/master/img/20210723144647.png)

> 在物理空间存在的位置

所有的表都存在data目录下

本质还是文件

MySQL 引擎在物理文件上的区别

![image-20210508094611463](https://gitee.com/feng2828/image/raw/master/img/20210723144648.png)

![image-20210508094932781](https://gitee.com/feng2828/image/raw/master/img/20210723144649.png)

## 2.6 修改删除表

> 修改

![image-20210508150515504](https://gitee.com/feng2828/image/raw/master/img/20210723144650.png)

> 删除

![image-20210508151438164](https://gitee.com/feng2828/image/raw/master/img/20210723144651.png)

 注意点

- ``  字段名。使用这个包裹
- 注释 --    /**/
- sql关键字不区分大小写，
- 所有的符号都用英文

# 3.MySQL数据管理

## 3.1 外键（了解即可）

![image-20210508154313809](https://gitee.com/feng2828/image/raw/master/img/20210723144652.png)

![image-20210508160604540](https://gitee.com/feng2828/image/raw/master/img/20210723144653.png)

![image-20210508160435744](https://gitee.com/feng2828/image/raw/master/img/20210723144654.png)

![image-20210508160638195](https://gitee.com/feng2828/image/raw/master/img/20210723144655.png)



## 3.2 DML数据管理语言（重要，记住）

### 3.2.1 添加

> insert

![image-20210508162142204](https://gitee.com/feng2828/image/raw/master/img/20210723144656.png)

```sql
INSERT INTO `student`(`id`,`name`,`age`) VALUES ('2','差距会','16'); 
```

添加一个字段多个数据时

```sql
INSERT INTO `student`(`name`) VALUES ('差距'),('hhh')
```

![image-20210508162945274](https://gitee.com/feng2828/image/raw/master/img/20210723144657.png)

**注意**

![image-20210508163725277](https://gitee.com/feng2828/image/raw/master/img/20210723144658.png)



### 3.2.2 修改

> update

![image-20210508164432555](https://gitee.com/feng2828/image/raw/master/img/20210723144659.png)

条件：where 子句 运算符

操作符会返回布尔值

![image-20210508165336527](https://gitee.com/feng2828/image/raw/master/img/20210723144700.png)

**语法**

![image-20210508165825707](https://gitee.com/feng2828/image/raw/master/img/20210723144701.png)



### 3.2.3 删除

> delete

语法 ：` delete from 表名 [where 条件]`

![image-20210508175020986](https://gitee.com/feng2828/image/raw/master/img/20210723144702.png)

![image-20210508192102618](https://gitee.com/feng2828/image/raw/master/img/20210723144703.png)

> TRUNCATE 命令 （**推荐使用**）

作用： 完全清空一个数据库的数据 ，表的结构和索引结构不会变！

```sql
truncate table tbl_name 或者 truncate tbl_name 。
```



> delete和TRUNCATE 的差异

- 相同点：都能删除数据，都不会删除表结构

- 不同

  ①TRUNCATE  重新设置 自增列 计数器归零

  ②TRUNCATE 不会影响事务

- 尽管truncate table与delete相似，但它被分类为DDL语句而不是DML语句。
- ![image-20210508180112959](https://gitee.com/feng2828/image/raw/master/img/20210723144704.png)
- ![image-20210508180622612](https://gitee.com/feng2828/image/raw/master/img/20210723144705.png)

> drop table和delete table的区别

* `drop`操作会删除所有的数据以及表结构

* `delete`操作会删除数据，但会保留表结构，并且在之后需要时可以回滚数据。此外，`delete`操作还可以加一些其它的`where`条件，比如删除确定的记录。

# 4.DQL查询数据（最重要）

## 4.1 概述

（data Query Language）:数据查询语言

- 所有的查询操作都用它 select
- 简单或者复杂的查询都用它
- `数据库中最核心的，最重要的语言，`
- 使用频率最高的语句

> 完整语法

```sql
select select_list

[into new_table_name]

from table_source

[where search_conditions]

[group by group_by_expression]

[having search_conditions]

[order by order_expression [ASC|DESC]]
[LIMIT {[offset,]row_count | row_countOFFSET offset}]
```

**into:**创建新表并将查询结果插入新表中。

new_table_name：表示保存查询结果的新表名。

**from:**table_source：指定查询的表或视图，派生表和联接表。

**where** :search_conditions：条件表达式，可以使关系表达式，也可以是逻辑表达式。

**group by**：将查询结果按指定的表达式分组。

group_by_expression：对其执行分组的表达式，group_by_expression也称为分组列，group_by_expression可以使列或引用列的非聚合表达式。

**having**：指定满足条件的组才予以输出。having通常与group by 的子句一起使用。

**order by**：指定结果集的排列顺序。

ASC：递增，从低到高排。

DESC：递减，从高到低排。

**limit**：[LIMIT {[offset,]row_count | row_countOFFSET offset}]

:指定查询记录从哪条到哪条，第一个是 0

这两个参数：起始值，页面的大小。

## 4.2 查询指定字段

 **1**、  别名，给字段取名字。就是在字段后加  AS  ，别名可以不打引号

```sql 
SELECT `studentno` AS 序号 , `studentresult` AS 成绩 FROM `result` 
```

**2.** 函数

```sql
-- 函数concat(a,b)
SELECT CONCAT('姓名:',studentname) AS 新名字 FROM `student`
```

![image-20210508200423396](https://gitee.com/feng2828/image/raw/master/img/20210723144706.png)

> 去重  distinct

作用：去除重复的数据，重复的数据只显示一条

![image-20210508201930608](https://gitee.com/feng2828/image/raw/master/img/20210723144707.png)



> 数据库的列

![image-20210508202309791](https://gitee.com/feng2828/image/raw/master/img/20210723144708.png)

`数据库中的表达式： 文本值，列，null ,函数，计算表达式，系统变量....`

 select ` 表达式` from 表

## 4.3 where 条件子句

作用：检索数据中` 符合条件`的值

搜索的条件由一个或者多个表达式组成！结果都是布尔值

> 逻辑运算符

![image-20210508203300825](https://gitee.com/feng2828/image/raw/master/img/20210723144709.png)

` 尽量使用英文字母`

![image-20210508203955379](https://gitee.com/feng2828/image/raw/master/img/20210723144710.png)



> 模糊查询 ：比较运算符

![image-20210508204424459](https://gitee.com/feng2828/image/raw/master/img/20210723144711.png)

**`like`** :使用LIKE运算选择类似的值，选择条件可以包含字符或数字：`%` 代表零个或多个字符(任意个字符)。`_ `代表一个字符

`IN`:是具体的一个或者多个值，不能像LIKE一样

```sql
-- 查询序号 1001 1002 1003 三个的成绩
SELECT `studentresult` FROM `result` WHERE  `studentno` IN (1001,1002,1003);
```

`null`: 代表为 NUll 或者是空字符串 ' '

![image-20210508210148849](https://gitee.com/feng2828/image/raw/master/img/20210723144712.png) 



## 4.4 联表查询

> join 对比

 ![image-20210508212040227](https://gitee.com/feng2828/image/raw/master/img/20210723144713.png)

![img](https://img-blog.csdnimg.cn/20181103160140252.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2h1YW5nX18y,size_16,color_FFFFFF,t_70)

```sql
SELECT s.studentno,studentname,studentresult FROM result AS r INNER JOIN student AS s 
WHERE s.studentno=r.studentno;
-- 这里where可以换成ON ，on代表批量，建立连接，where代表单个，对结果的筛选
-- join（连接的表） on（判断条件 ） 连接查询
-- where 等值查询
```

inner join：匹配的都查出来，并集匹配的

left join:左边的加上交集

right join :右边的加上你交集

![image-20210512152854595](https://gitee.com/feng2828/image/raw/master/img/20210723144714.png)

` 思路`![image-20210512154510096](https://gitee.com/feng2828/image/raw/master/img/20210723144715.png)

> 自连接

`自己的表和自己的表连接，核心：一张表拆为两张一样的表即可`

原表

![image-20210513102331503](https://gitee.com/feng2828/image/raw/master/img/20210723144716.png)



![image-20210513101348598](https://gitee.com/feng2828/image/raw/master/img/20210723144717.png)

![image-20210513101530697](https://gitee.com/feng2828/image/raw/master/img/20210723144718.png)

![image-20210513101714681](https://gitee.com/feng2828/image/raw/master/img/20210723144719.png)

![image-20210513102429801](https://gitee.com/feng2828/image/raw/master/img/20210723144720.png)

## 4.5 分页和排序

> 分页

作用：缓解数据库压力，体验更好

![image-20210513110037612](https://gitee.com/feng2828/image/raw/master/img/20210723144721.png)

![image-20210513110114948](https://gitee.com/feng2828/image/raw/master/img/20210723144722.png)

**语法**：`limit(查询起始下标，pageSize)`

> 排序

![image-20210513110346572](https://gitee.com/feng2828/image/raw/master/img/20210723144723.png)

## 4.6 子查询

where(这个值·是计算出来的)        **由里及外**

` 本质`：在where里面嵌套一个子查询语句

where（select * from ....）

![image-20210517195852366](https://gitee.com/feng2828/image/raw/master/img/20210723144724.png)

**嵌套查询**

![image-20210517195613846](https://gitee.com/feng2828/image/raw/master/img/20210723144725.png)

# 5.MySQL函数

官网地址：https://www.runoob.com/mysql/mysql-functions.html

## 5.1 常用函数

![image-20210517200930830](https://gitee.com/feng2828/image/raw/master/img/20210723144726.png)

![image-20210517202903491](https://gitee.com/feng2828/image/raw/master/img/20210723144727.png)

![image-20210517203058562](https://gitee.com/feng2828/image/raw/master/img/20210723144728.png)

`时间和日期函数（记住）`

![image-20210517203457742](https://gitee.com/feng2828/image/raw/master/img/20210723144729.png)

![image-20210517203559301](https://gitee.com/feng2828/image/raw/master/img/20210723144730.png)



## 5.2 聚合函数（经常用）

![image-20210517203800021](https://gitee.com/feng2828/image/raw/master/img/20210723144731.png)

`count()`

![image-20210517204700900](https://gitee.com/feng2828/image/raw/master/img/20210723144732.png)

1.比较count(*)和count(字段名)的区别：前者对行的数目进行计算，包含null，后者对特定的列的值具有的行数进行计算，不包含null，得到的结果将是除去值为null和重复数据后的结果。

2.count（1）跟count（主键）一样，只扫描主键

3.count（*）和count（主键）使用方式一样，但是在性能上有略微的区别，mysql对前者做了优化。

count（主键）不一定比count（其余索引快）。

count（字段），该字段非主键的情况最好不要出现，因为该方式不走索引。

一、count (**)和count(1)
使用count函数，当要统计的数量比较大时，发现count(*)花费的时间比较多，相对来说count(1)花费的时间比较少。

1、如果你的数据表**没有主键**，那么count(1)比count(*)快 ；如果有主键的话，那主键（联合主键）作为count的条件也比count(*)要快  。

2、如果你的表**只有一个字段**的话那count(*)就是最快的。  

3、如果count(1)是聚索引,id,那肯定是count(1)快,但是差的很小的。因为count(*),自动会优化指定到那一个字段。所以没必要去count(1)，用count(*),sql会帮你完成优化。此时count(1)和count(*)基本没有区别! 

二、count(*) 和count(列名) 两者查询差异的原因分析。
在数据记录都不为空的时候查询出来结果上没有差别的.

count(*)（**是针对全表**）将返回表格中所有存在的行的总数包括值为null的行；

count(列名)（**是针对某一列**）将返回表格中某一列除去null以外的所有行的总数。

`分组`  **GROUP BY **

![image-20210517211117819](https://gitee.com/feng2828/image/raw/master/img/20210723144733.png)

`having`                           **用了聚和函数就要用having**                                                                                **👇**

![image-20210518160418721](https://gitee.com/feng2828/image/raw/master/img/20210723144734.png)

`having和where的区别`

- where子句**作用于基本表或者视图**，从而选择满足条件的元组
- Having 短语**作用于组**，从而选择满足条件的组



## 5.3 数据库级别的MD5加密（扩展）

MD5 :`主要增加算法复杂度和不可逆性`

![image-20210518161909508](https://gitee.com/feng2828/image/raw/master/img/20210723144735.png)



## 5.4 小结

![image-20210518162602324](https://gitee.com/feng2828/image/raw/master/img/20210723144736.png)

 

# 6.事务

## 6.1 什么是事务

`要么都成功，要么都失败`   

例如：转账

![image-20210518163353859](https://gitee.com/feng2828/image/raw/master/img/20210723144737.png)



`事务原则：`

>ACID原则：原子性，一致性，隔离性，持久性     （脏读，幻读）

**原子性（Atomicity）**  ：指事务是数据库工作的最小单位，一个事务中的所有操作要么全部成功提交，要么全部失败回滚

**一致性（Consistency） ** ：指事务操作不能破坏数据的一致性，数据库在一个事务的执行前后都应处于一致性状态。**最终一致性和过程一致性**

类似于能量守恒定律

**隔离性（Isolation）**  ：指数据库并发情况下，并发的事务直接是隔离的，**一个事务的执行不能被其他事务影响**。隔离性比较复杂，在MySQL中存在四种隔离级别：Read Uncommitted、Read Committed、Repeatable Read和Serializable，不同的隔离级别采用不同的锁机制实现
**持久性（Durability）**  ：指一旦事务提交，则其对数据的变更就是永久性的（不可逆 ），即使数据库发生任何故障都不应该对数据造成任何影响。**事务没有提交恢复到原状，事务已经提交持久化到数据库不会改变**

`存在干扰的情况下`

**①脏读：**

![image-20210518165041178](https://gitee.com/feng2828/image/raw/master/img/20210723144738.png)

**②不可重复读**

![image-20210518165133612](https://gitee.com/feng2828/image/raw/master/img/20210723144739.png)

**③虚读（幻读）**

![image-20210518165226357](https://gitee.com/feng2828/image/raw/master/img/20210723144740.png)



> 执行事务

MySQL是默认开启事务自动提交的

**执行流程**

![image-20210518190903964](https://gitee.com/feng2828/image/raw/master/img/20210723144741.png)

**事务的一些SQL语句**

![image-20210518191117751](https://gitee.com/feng2828/image/raw/master/img/20210723144742.png)



> 模拟场景

```sql

CREATE DATABASE shop CHARACTER SET utf8 COLLATE utf8_general_ci; --建库

USE shop
-- 建表
CREATE TABLE `account` ( 
    `id` INT (10) NOT NULL AUTO_INCREMENT, 
     `name` VARCHAR (20) NOT NULL, 
    `money` DECIMAL (9,2) NOT NULL,
    PRIMARY KEY (`id`)
)ENGINE=INNODB DEFAULT CHARSET utf8; 
-- 插数据
INSERT INTO `account` (`name`,`money`) VALUES ('憨憨','2000'),('憨','10000')

-- 模拟转账 ；事务
SET autocommit=0;-- 关闭自动提交
START TRANSACTION;-- 开启一个事务 （一组事务）
UPDATE account SET  money=money-500 WHERE NAME = '憨憨';-- 憨憨减500
UPDATE account SET  money=money+500 WHERE NAME = '憨'; -- 憨加500
COMMIT;-- 提交事务，数据被持久化，回滚不了了
ROLLBACK; -- 失败回滚
SET autocommit=1;-- 恢复自动提交

```

# 7.索引

> 索引（index）是对数据库表中一列或多列的值进行排序的一种数据结构。MySQL索引的建立对于MySQL的高效运行是很重要的，索引可以大大提高MySQL的检索速度。索引只是提高效率的一个因素，如果你的MySQL有大数据量的表，就需要花时间研究建立最优秀的索引，或优化查询语句。

## 7.1 索引的分类

> 在一个表中，主键索引只能有一个，唯一索引可以有多个

- 主键索引（PRIMARY KEY）
  - 唯一的标识，主键不可重复，只能有一个 字段作为主键
- 唯一索引（UNIQUE KEY ）
  - 避免重复的列出现（避免名字相同），唯一索引可以重复即多个列可以是唯一索引的标识
- 常规索引（KEY或者INDEX）
  - 默认的，可以用index或者key关键字设置
- 全文索引（FULLTEXT）
  - 在特定的数据库引擎才有，MYISAM（现在更新后应该都有）
  - 快速定位数据

**索引的使用**

1.在创建表的时候给字段增加索引

```sql
CREATE TABLE `student`(
	`studentno` INT(4) NOT NULL COMMENT '学号',
    `loginpwd` VARCHAR(20) DEFAULT NULL,
    `studentname` VARCHAR(20) DEFAULT NULL COMMENT '学生姓名',
    `sex` TINYINT(1) DEFAULT NULL COMMENT '性别，0或1',
    `gradeid` INT(11) DEFAULT NULL COMMENT '年级编号',
    `phone` VARCHAR(50) NOT NULL COMMENT '联系电话，允许为空',
    `address` VARCHAR(255) NOT NULL COMMENT '地址，允许为空',
    `borndate` DATETIME DEFAULT NULL COMMENT '出生时间',
    `email` VARCHAR (50) NOT NULL COMMENT '邮箱账号允许为空',
    `identitycard` VARCHAR(18) DEFAULT NULL COMMENT '身份证号',
    --  这里
    PRIMARY KEY (`studentno`),
    UNIQUE KEY `identitycard`(`identitycard`),
    KEY `email` (`email`)
)ENGINE=MYISAM DEFAULT CHARSET=utf8;
```



2.创建完毕后，增加索引

**格式**  ... 索引名 （列名）

![image-20210518201910460](https://gitee.com/feng2828/image/raw/master/img/20210723144743.png)



## 7.2 测试索引

**MySql数据库中在创建时间字段的时候**

①DEFAULT CURRENT_TIMESTAMP
表示当插入数据的时候，该字段默认值为当前时间

②当执行update操作是，并且字段有ON UPDATE CURRENT_TIMESTAMP属性。则字段无论值有没有变化，它的值也会跟着更新为当前UPDATE操作时的时间。

```sql

CREATE TABLE `app_user` (
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` VARCHAR(50) DEFAULT '' COMMENT'用户昵称',
`email` VARCHAR(50) NOT NULL COMMENT '用户邮箱',
`phone` VARCHAR(20) DEFAULT '' COMMENT '手机号',
`gender` TINYINT(4) UNSIGNED DEFAULT '0' COMMENT '性别（0：男;1:女）',
`password` VARCHAR(100) NOT NULL COMMENT '密码',
`age` TINYINT(4) DEFAULT '0'  COMMENT '年龄',
`create_time` DATETIME DEFAULT CURRENT_TIMESTAMP,
`update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT = 'app用户表';

-- 插入100万数据.  25.547 sec
DELIMITER $$  -- 写函数之前必须要写，标志
CREATE FUNCTION mock_data ()  -- 模拟数据
RETURNS INT
DETERMINISTIC   -- 8.0版本加上这一行
BEGIN
	DECLARE num INT DEFAULT 1000000; 
	DECLARE i INT DEFAULT 0; -- 起始变量
	WHILE i<num DO
		-- 循环体内写插入语句
		INSERT INTO `app_user`(`name`,`email`,`phone`,`gender`,`password`,`age`)
		-- (CONCAT('用户',i)  CONCAT用于动态改变 
		-- RAND()*(999999999-100000000) 100000000到999999999的随机数
		-- FLOOR 向下取整
		VALUES(CONCAT('用户',i),'19224305@qq.com',CONCAT('18',FLOOR(RAND()*((999999999-100000000)+100000000))),
		FLOOR(RAND()*2),UUID(),FLOOR(RAND()*100));
		SET i=i+1;
	END WHILE;
	RETURN i;
END;

SELECT mock_data() -- 执行此函数 生成一百万条数据


SELECT * FROM `app_user` WHERE `name` = '用户9999' -- 0.833 sec

EXPLAIN SELECT * FROM `app_user` WHERE `name` = '用户9999' -- row 991611

-- 索引命名方式  id_表名_字段名
-- CREATE PRIMARY KEY/UNIQUE KEY/FULLTEXT  INDEX 索引名 on 表（字段）
CREATE INDEX id_app_user_name ON app_user(`name`);

SELECT * FROM `app_user` WHERE `name` = '用户9999' -- 0.009 sec


EXPLAIN SELECT * FROM `app_user` WHERE `name` = '用户9999' -- row 1
```

`DETERMINISTIC`:**它表示一个函数在输入不变的情况下，函数的返回值完全由输入参数决定**。如果你的函数当输入一样时,会返回同样的结果.这样, 数据库就用前一个计算的值,而不需要再重新计算一次.这对于使用函数索引等,会直到相当大的好处.

***\*确定性函数有以下用处：\****

1.可以在基于函数的索引中使用该函数；

2.可以在物化视图中调用；

3.Oracle11g开始会对其参数及其返回结果进行缓存处理以提升性能。

但是，是不是一个确定性函数是需要用户来负责的，就是说对函数进行编译的时候不会检查出这个函数是否是确定性的。

像oracle的内置函数UPPER,TRUNC等都是deterministic函数，而像DBMS_RANDOM.VALUE就不是deterministic函数，因为同样的输入不一定会导致同样的输出。

原因分析: `日志`

因为CREATE PROCEDURE, CREATE FUNCTION, ALTER PROCEDURE,ALTER FUNCTION,CALL, DROP PROCEDURE, DROP FUNCTION等语句都会被写进二进制日志,然后在从服务器上执行。那么为什么MySQL有这样的限制呢？ 因为二进制日志的一个重要功能是用于主从复制，而存储函数有可能导致主从的数据不一致。所以当开启二进制日志后，参数log_bin_trust_function_creators就会生效，限制存储函数的创建、修改、调用。**但是，一个执行更新的不确定子程序(存储过程、函数、触发器)是不可重复的，在从服务器上执行(相对与主服务器是重复执行)可能会造成恢复的数据与原始数据不同，从服务器不同于主服务器的情况。**

为了解决这个问题，`MySQL强制要求`：

**在主服务器上，除非子程序被声明为确定性的或者不更改数据，否则创建或者替换子程序将被拒绝。**

**这意味着当创建一个子程序的时候，必须要么声明它是确定性的，要么它不改变数据。**

当二进制日志启用后，这个变量就会启用。它控制是否可以信任存储函数创建者，不会创建写入二进制日志引起不安全事件的存储函数。如果设置为0(默认值)，用户不得创建或修改存储函数，除非它们具有除CREATE ROUTINE或ALTER ROUTINE特权之外的SUPER权限。 设置为0还强制使用DETERMINISTIC特性或READS SQL DATA或NO SQL特性声明函数的限制。 如果变量设置为1，MySQL不会对创建存储函数实施这些限制。 此变量也适用于触发器的创建。


声明方式有两种,

第一种:声明是否是确定性的

DETERMINISTIC和NOT DETERMINISTIC指出一个子程序是否对给定的输入总是产生同样的结果。

如果没有给定任一特征，默认是NOT DETERMINISTIC，所以必须明确指定DETERMINISTIC来声明一个子程序是确定性的。

这里要说明的是:使用NOW() 函数(或它的同义)或者RAND() 函数不会使一个子程序变成非确定性的。对NOW()而言，二进制日志包括时间戳并会被正确的执行。RAND()只要在一个子程序内被调用一次也可以被正确的复制。所以，www.linuxidc.com可以认为时间戳和随机数种子是子程序的确定性输入，它们在主服务器和从服务器上是一样的。

第二种:声明是否会改变数据

CONTAINS SQL, NO SQL, READS SQL DATA, MODIFIES SQL用来指出子程序是读还是写数据的。

无论NO SQL还是READS SQL DATA都指出，子程序没有改变数据，但是必须明确地指定其中一个，因为如果任何指定，默认的指定是CONTAINS SQL。

默认情况下，如果允许CREATE PROCEDURE 或CREATE FUNCTION 语句被接受，就必须明确地指定DETERMINISTIC 或 NO SQL与READS SQL DATA 中的一个，否则就会产生1418错误。



`第三种索引建立方式`

```sql
-- 索引命名方式  id_表名_字段名
-- CREATE PRIMARY KEY/UNIQUE KEY/FULLTEXT  INDEX 索引名 on 表（字段）
CREATE INDEX id_app_user_name ON app_user(`name`);
```

**索引在小数据量的时候，感知不大，但是在大数据的时候，区别十分明显**

## 7.3 索引原则

- 索引不是越多越好
- 不要对进程变动数据加索引
- 小数据量的表不需要加索引
- 索引一般加在常用来查询的字段上

> 索引的数据结构

Hash类型的索引

BTree: INNODB默认的数据结构

# 8.数据库权限管理和备份

## 8.1 权限管理

> SQL yog 可视化管理

![image-20210519152101224](https://gitee.com/feng2828/image/raw/master/img/20210723144744.png)

> SQL命令操作

用户表：mysql.user

![image-20210519152341085](https://gitee.com/feng2828/image/raw/master/img/20210723144745.png)

**具体代码**

![image-20210519192019158](https://gitee.com/feng2828/image/raw/master/img/20210723144746.png)



## 8.2 MySQL备份

> 作用

- 保证重要的数据不丢失
- 数据转移

> 备份方式

- 直接拷贝物理文件

![image-20210519192539546](https://gitee.com/feng2828/image/raw/master/img/20210723144747.png)

- 在SQLyog这种可视化工具中手动导出
  - 在想要导出的库或者表中，右键，选择备份或者导出

![image-20210519193140190](https://gitee.com/feng2828/image/raw/master/img/20210723144748.png)

- 使用命令行 mysqldump 命令行使用

`配置系统变量的情况下`

![image-20210519194535837](https://gitee.com/feng2828/image/raw/master/img/20210723144749.png)

**示例**

![image-20210519194051357](https://gitee.com/feng2828/image/raw/master/img/20210723144750.png)

`没有配置系统环境时要进入bin目录下使用mysqldump`

![image-20210519195719670](https://gitee.com/feng2828/image/raw/master/img/20210723144751.png)

![image-20210519195801771](https://gitee.com/feng2828/image/raw/master/img/20210723144752.png)

# 9.规范数据库设计

## 9.1 为什么需要设计

`当数据库比较复杂的时候，我们就需要设计了`

![image-20210519200714514](https://gitee.com/feng2828/image/raw/master/img/20210723144753.png)

**设计数据库的步骤**

![image-20210519204249793](https://gitee.com/feng2828/image/raw/master/img/20210723144754.png)



## 9.2 三大范式

![image-20210519204520675](https://gitee.com/feng2828/image/raw/master/img/20210723144755.png)

> 三大范式

数据库的设计范式是数据库设计所需要满足的规范，满足这些规范的数据库是简洁的、结构明晰的，同时，不会发生插入（insert）、删除（delete）和更新（update）操作异常。

**一.第一范式（1NF）:列不可再分**
1.每一列属性都是不可再分的属性值，确保每一列的`原子性`

2.两列的属性相近或相似或一样，尽量合并属性一样的列，确保不产生冗余数据

![20180815162251538](E:\没用的文件\OneDrive\桌面\20180815162251538.png)

 **二.第二范式（2NF）属性完全依赖于主键**  （每张表只描述一件事情）
第二范式（2NF）是**在第一范式（1NF）的基础上建立起来的**，即满足第二范式（2NF）必须先满足第一范式（1NF）。第二范式（2NF）要求数据库表中的每个实例或行必须可以被惟一地区分。为实现区分通常需要为表加上一个列，以存储各个实例的惟一标识。这个惟一属性列被称为主键

![20180815162336295](E:\没用的文件\OneDrive\桌面\20180815162336295.png)

![20180815162400341](E:\没用的文件\OneDrive\桌面\20180815162400341.png)

**三.第三范式（3NF）属性不依赖于其它非主属性，属性直接依赖于主键**
数据不能存在传递关系，即每个属性都跟主键有直接关系而不是间接关系。像：a-->b-->c  属性之间含有这样的关系，是不符合第三范式的。

比如Student表（学号，姓名，年龄，性别，所在院校，院校地址，院校电话）

这样一个表结构，就存在上述关系。 学号--> 所在院校 --> (院校地址，院校电话)

这样的表结构，我们应该拆开来，如下。

   （学号，姓名，年龄，性别，所在院校）--（所在院校，院校地址，院校电话）

`总结`：三大范式只是一般设计数据库的基本理念，可以建立冗余较小、结构合理的数据库。如果有特殊情况，当然要特殊对待，数据库设计最重要的是看需求跟性能，**需求>性能>表结构**。所以不能一味的去追求范式建立数据库。
![image-20210519210241864](https://gitee.com/feng2828/image/raw/master/img/20210723144756.png)



# 10.JDBC(重点)

## 10.1 数据库驱动

驱动：声卡，显卡，数据库

![image-20210519210730602](https://gitee.com/feng2828/image/raw/master/img/20210723144757.png)

我们的程序就是通过数据库驱动和数据库打交道

## 10.2 JDBC

sun公司为了简化 开发人员的（对数据库的统一）操作，提供了一个（Java操作数据库）的规范，俗称JDBC,这些规范的实现由具体的厂商去做。

对于开发人员，只需要掌握JDBC接口的操作即可

![image-20210519211401286](https://gitee.com/feng2828/image/raw/master/img/20210723144758.png)

java.sql

javax.sql

还需要导入一个数据库驱动包

下载地址：https://mvnrepository.com/artifact/mysql/mysql-connector-java

**查询MySQL数据库版本**

打开mysql在命令提示符上输入 select version();

![image-20210519211946880](https://gitee.com/feng2828/image/raw/master/img/20210723144759.png)

## 10.3 第一个JDBC程序

> 创建测试数据库

```sql
CREATE DATABASE `jdbcStudy` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `jdbcStudy`;

CREATE TABLE `users`(
 `id` INT PRIMARY KEY,
 `NAME` VARCHAR(40),
 `PASSWORD` VARCHAR(40),
 `email` VARCHAR(60),
 `birthday` DATE
);

 INSERT INTO `users`(`id`,`NAME`,`PASSWORD`,`email`,`birthday`)
VALUES(1,'zhangsan','123456','zs@sina.com','1980-12-04'),
(2,'lisi','123456','lisi@sina.com','1981-12-04'),
(3,'wangwu','123456','wangwu@sina.com','1979-12-04');
```

1.创建一个普通项目

2.导入数据库驱动

![image-20210520150255610](https://gitee.com/feng2828/image/raw/master/img/20210723144800.png)

3.编写测试代码(**注意MySQL版本**)

```java
package com.f.lesson01;

import java.sql.*;

public class JDBCFirst {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //1.加载驱动
        Class.forName("com.mysql.cj.jdbc.Driver");//固定写法 8.0的要加  cj
           //DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver() );//上面这行代码本质是这行代码，作			用是注册驱动,
        //2.用户信息和url
        String url="jdbc:mysql://localhost:3306/jdbcstudy?serverTimezone=UTC&useUnicode=true&characterEncoding=utf8&useSSL=true";
        //8.0版本要加上时区
        String username="root";
        String password="123456";
            //serverTimezone=UTC ：时区
            //useUnicode=true :支持中文编码
            //characterEncoding=utf8 : 字符集为utf-8
            //useSSL=true :使用安全的连接，防止异常的错误造成的异常
        //3.连接成功，数据库对象
        Connection con = DriverManager.getConnection(url, username, password);
            //DriverManager驱动管理类
            //Connection 代表数据库
        // 4.执行SQL的对象
        Statement statement = con.createStatement();
            //Statement 执行SQL的对象

        //5.执行SQL的对象去执行SQL，可能存在结果，查看返回结果
        String sql="SELECT * FROM users;";//要执行的sql语句
        ResultSet resultSet = statement.executeQuery(sql);//resultSet 返回的结果集,结果集中封装了我们全部的查询出来的结果
        while (resultSet.next()){
            System.out.println("id="+resultSet.getObject("id"));
            System.out.println("name="+resultSet.getObject("NAME"));
            System.out.println("pwd="+resultSet.getObject("PASSWORD"));
            System.out.println("email="+resultSet.getObject("email"));
            System.out.println("birth="+resultSet.getObject("birthday"));
            System.out.println("--------------");
        }//resultSet.next()  :指针指向下一条记录，有记录（有值）返回true并把记录内容存入到对应的对象中，也就是obj.next()的obj中。如果没有返回false。
        //6.释放连接
        resultSet.close();
        statement.close();
        con.close();
    }
}

```

步骤总结

1.连接驱动

2.连接数据库DriverManager

3.获取执行sql的对象  Statement

4.获得返回的结果集

5.释放连接

> DriverManager

```
Class.forName(xxx.xx.xx)返回的是一个类
Class.forName(xxx.xx.xx)的作用是要求JVM查找并加载指定的类，
也就是说JVM会执行该类的静态代码段
```

```java
//DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver() );//注册驱动,
 Class.forName("com.mysql.cj.jdbc.Driver");//会加载Driver这个类，然后执行
 DriverManager.registerDriver(new Driver());//来注册驱动
```

```java
Connection con = DriverManager.getConnection(url, username, password);
//con 代表数据库
//con.commit();//提交
//con.rollback();//回滚
```

> URL  

![image-20210520161033301](https://gitee.com/feng2828/image/raw/master/img/20210723144801.png)

```java
        String url="jdbc:mysql://localhost:3306/jdbcstudy?serverTimezone=UTC&useUnicode=true&characterEncoding=utf8&useSSL=true";

//协议://主机地址：端口号/数据库名？参数1&参数2&参数3&参数4
//mysql --3306

//oracle --1521
//jdbc:oracle:thin:@localhost:1521:sid
```



> Statement (执行SQL的对象)    prepareStatement也是

```java
statement.executeQuery();//查询操作返回ResultSet
statement.execute();//执行任何SQL,需要判断，效率低一点
statement.executeUpdate();//更新，插入，删除都是用这个，返回一个受影响的行数（几行改变了就返回几行）
```

> ResultSet 查询的结果集：封装了所有的查询结果

获得指定的数据类型 

```java
resultSet.getObject();//在不知道列类型的情况下使用
resultSet.getString();//如果知道列的类型就使用指定的类型
resultSet.getInt();
resultSet.getFloat();
resultSet.getDate();
.....
```

遍历，指针

![image-20210520163640604](https://gitee.com/feng2828/image/raw/master/img/20210723144802.png)

![image-20210520163914761](https://gitee.com/feng2828/image/raw/master/img/20210723144803.png)



> 释放资源

```java
resultSet.close();
statement.close();
con.close();//耗资源，用完关掉
```



### 10.3.1 扩展 静态代码块

> 静态代码块

**随着类的加载而执行，而且只执行一次**

 **静态代码块**：执行优先级高于非静态的初始化块，它会在类初始化的时候执行一次，**执行完成便销毁**，它仅能初始化类变量，即static修饰的数据成员。

静态代码块写法，

static{

}

**非静态代码块**：

执行的时候如果有静态初始化块，先执行静态初始化块再执行非静态初始化块，在每个对象生成时都会被执行一次，它可以初始化类的实例变量。非静态初始化块会在构造函数执行时，在构造函数主体代码执行之前被运行。

写法：

{

}

在类中定义一个静态代码块就行了，然后在里面写对应的代码即可

![img](https://img-blog.csdnimg.cn/20190417174029166.png)

静态代码块的执行顺序：`**静态代码块----->非静态代码块-------->构造函数`**

但是面试中，面试官可能会连着其他知识点一起问，比如说继承，这边我就写个小列子，

`静态代码块----->main函数(sout输出的)---> 非静态代码块-------->构造函数`（**这三个都是先父类再子类**）

父类：![img](https://img-blog.csdnimg.cn/20190417174538450.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1ODY4NDEy,size_16,color_FFFFFF,t_70)



子类：![img](https://img-blog.csdnimg.cn/20190417174554482.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1ODY4NDEy,size_16,color_FFFFFF,t_70)

在子类中执行main方法，可以看到控制台打印出来的结果

![img](https://img-blog.csdnimg.cn/20190417174633290.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1ODY4NDEy,size_16,color_FFFFFF,t_70)

如果你想看静态代码块的特征，**随着类的加载而执行，而且只执行一次**，就在父类中new一个子类就可以看出来了。

![img](https://img-blog.csdnimg.cn/20190417174913631.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1ODY4NDEy,size_16,color_FFFFFF,t_70)

执行父类中的main方法，控制台打印

![img](https://img-blog.csdnimg.cn/20190417175124130.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1ODY4NDEy,size_16,color_FFFFFF,t_70)

然后跟上面的在子类中new一个sons类的结果进行对比，左图是父类执行main方法，右图是子类执行main方法，

可以看出来，原本在子类中执行main方法，由于子类继承父类，所以父类中的静态代码块优先执行一次，

但是在左图中，是在父类中执行了main方法，本身父类执行main方法就会执行一次静态代码块，但是在父类中main方法new了一次子类，按继承关系，父类中的静态代码块应该还会执行，但是控制台中却没有打印，这就是因为静态代码块的特征的原因所致，随着类的加载而执行，而且只执行一次，

如果还是不理解，以下代码更为直观
![img](https://img-blog.csdnimg.cn/20190417180450630.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1ODY4NDEy,size_16,color_FFFFFF,t_70)

### 10.3.2 时间戳

时间戳（timestamp）：

通常是一个字符序列，唯一地标识某一刻的时间。数字时间戳技术是数字签名技术一种变种的应用。 定义：   时间戳是指格林威治时间1970年01月01日00时00分00秒(北京时间1970年01月01日08时00分00秒)起至现在的总秒数

 

## 10.4 Statement 对象

jdbc中的statement对象用于向数据库发送SQL语句，想完成对数据库的增删改查，只需要通过这个对象向数据库发送增删改查语句即可。

```java
statement.executeQuery();//查询操作返回ResultSet
statement.execute();//执行任何SQL,需要判断，效率低一点
statement.executeUpdate();//更新，插入，删除都是用这个，返回一个受影响的行数（几行改变了就返回几行）
```

> CRUD操作-create

使用executeUpdate（String sql）方法完成数据添加操作，示例操作：

```java
Statement st = conn.createStatement();
String sql = "insert into user(...) values(....)";
int num = st.executeUpdate(sql);
if(num>0){
	System.out.println("插入成功！");
}

```

> CRUD操作 – delete

使用executeUpdate(String sql)方法完成数据删除操作

```java
Statement st = conn.createStatement();
String sql = "delete from user where id = 1";
int num = st.executeUpdate(sql);
if(num>0){
	System.out.println("删除成功！");
}

```

> CRUD操作 – update

使用executeUpdate(String sql)方法完成数据修改操作

```java
Statement st = conn.createStatement();
String sql = "update user set name = '' where name = '' ";
int num = st.executeUpdate(sql);
if(num>0){
	System.out.println("修改成功！");
}

```

> CRUD操作 – read

使用executeQuery(String sql)方法完成数据库查询操作

```java
Statement st = conn.createStatement();
String sql = " select * from user where id = 1 ";
int num = st.executeQuery(sql);
while(rs.next()){
	//根据获取列表的数据类型，分别调用rs的相应方法映射到JAVA对象中
}
```

> **插入数据程序实例**

新建db.properties文件，存放连接数据库所用参数

**注意是在src包上创建文件**

![image-20210520170651309](https://gitee.com/feng2828/image/raw/master/img/20210723144804.png)

![image-20210520170710150](https://gitee.com/feng2828/image/raw/master/img/20210723144805.png)

db.properties文件

```java
Driver=com.mysql.cj.jdbc.Driver
url=jdbc:mysql://localhost:3306/jdbcstudy?serverTimezone=UTC&useUnicode=true&characterEncoding=utf8&useSSL=true
username=root
password=123456

```

新建工具类，写有连接数据库使用的代码，方便其他类调用。

Properties 是实现MAP集合的类，map集合是键值对，然后通过getClassLoader得到流对象，

![image-20210520171801029](https://gitee.com/feng2828/image/raw/master/img/20210723144806.png)

jdbcUtils.java

```java
package com.f.lesson02.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class jdbcUtils {
    private static String driver=null;
    private static String url=null;
    private static String username=null;
    private static String password=null;
    static {
        try {
            InputStream in = jdbcUtils.class.getClassLoader().getResourceAsStream("db.properties");
            Properties properties = new Properties();
            properties.load(in);
            String driver = properties.getProperty("Driver");
            String url = properties.getProperty("url");
            String username = properties.getProperty("username");
            String password = properties.getProperty("password");

            //1.驱动只需要加载一次
            Class.forName(driver);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    //2.获取连接
    public static Connection getConnection() throws SQLException {
         return DriverManager.getConnection(url, username, password);

    }
    //3.释放连接资源
    public static void release(Connection conn, Statement st, ResultSet re)  {
        if(re!=null){
            try {
                re.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if(st!=null){
            try {
                st.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}

```

测试插入类

![image-20210520174009278](https://gitee.com/feng2828/image/raw/master/img/20210723144807.png)

TestInsert.java

```java
package com.f.lesson02;

import com.f.lesson02.utils.jdbcUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestInsert {
    public static void main(String[] args) {
        //try里面这个鞋东西是局部变量，然后全局声明提高范围，不然这个资源释放不掉
        Connection conn=null;
        Statement st=null;
        ResultSet rs=null;
        try {
            conn= jdbcUtils.getConnection();//获取数据库连接
            st = conn.createStatement();//获得SQL执行对象
            String sql="INSERT INTO `users`(id,`NAME`,`PASSWORD`,`email`,`birthday`)VALUES(4,'jxkn','12345','cha@qq.com','1958-12-02')"; //改一下sql语句就可以用增删改
            int i = st.executeUpdate(sql);
            if(i>0){
                System.out.println("插入成功！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            jdbcUtils.release(conn,st, null);
        }
    }
}

```

> 查询

```java
package com.f.lesson02;

import com.f.lesson02.utils.jdbcUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestSelect {
    public static void main(String[] args) {
        Connection conn=null;
        Statement st=null;
        ResultSet rs=null;
        try{
            try {
                conn = jdbcUtils.getConnection();
                st = conn.createStatement();
                String sql="SELECT * FROM users WHERE id=1;";
                rs = st.executeQuery(sql);//返回一个结果集
                //通过while来遍历
                while (rs.next()){
                    System.out.println(rs.getString("NAME"));
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }finally {
            jdbcUtils.release(conn,st,rs);
        }
    }
}

```

> SQL注入的问题

sql存在漏洞，会被攻击导致数据泄露 ` SQL会被拼接  or`

定义：SQL注入即是指[web应用程序](https://baike.baidu.com/item/web应用程序/2498090)对用户输入数据的合法性没有判断或过滤不严，攻击者可以在web应用程序中事先定义好的查询语句的结尾上添加额外的[SQL语句](https://baike.baidu.com/item/SQL语句/5714895)，在管理员不知情的情况下实现非法操作，以此来实现欺骗[数据库服务器](https://baike.baidu.com/item/数据库服务器/613818)执行非授权的任意查询，从而进一步得到相应的数据信息。

```java
package com.f.lesson02;

import com.f.lesson02.utils.jdbcUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQL注入 {
    public static void main(String[] args) {
       // login("zhangsan","123456");正常登录
        login(" 'or'1=1"," 'or'1=1");//SQL拼接注入
    }
    public static void login(String username,String password){
        Connection conn=null;
        Statement st=null;
        ResultSet rs=null;
        try{
            try {
                conn = jdbcUtils.getConnection();
                st = conn.createStatement();
                //SELECT * FROM users WHERE `name`='zhangsan'AND `password`='123456';
                //注意拼接方法
                String sql="SELECT * FROM users WHERE `NAME`='"+username+"'and`PASSWORD`='"+password+"';";
                rs = st.executeQuery(sql);//返回一个结果集
                //通过while来遍历
                while (rs.next()){
                    System.out.println(rs.getString("NAME"));
                    System.out.println(rs.getString("PASSWORD"));
                    System.out.println("===================");
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }finally {
            jdbcUtils.release(conn,st,rs);
        }
    }


    }


```

## 10.5 PreparedStatement对象

PreparedStatement 可以防止SQL注入，效率更高

`PreparedStatement 可以防止SQL注入的本质：把传递进来的参数当作字符假设其中存在转义字符，就直接忽略 ，比如‘ 会被直接转义   `

> 新增

```java
package com.f.lesson03;

import com.f.lesson02.utils.jdbcUtils;

import java.sql.*;
import java.util.Date;

public class TestInsert {
    public static void main(String[] args) {

        Connection conn=null;
        PreparedStatement st=null;
        ResultSet rs=null;


        try {
             conn = jdbcUtils.getConnection();

             //区别
            //使用？ 占位符代替参数
            String sql="INSERT INTO `users`(id,`NAME`,`PASSWORD`,`email`,`birthday`)values(?,?,?,?,?)";
            st=conn.prepareStatement(sql);//预编译SQL,先写SQL,然后不执行
            //手动给参数赋值
            st.setInt(1,5);  //?下标从1开始
            st.setString(2,"hhh");
            st.setString(3,"12345");
            st.setString(4,"cac@qq.com");
            //注意点 sql.Date 数据库用的date
            //      util.Date java用的date   new Date().getTime() 获得时间戳
            st.setDate(5,new java.sql.Date(new Date().getTime()));

            //执行
            int i = st.executeUpdate();
            if(i>0){
                System.out.println("插入成功！");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            jdbcUtils.release(conn,st,null);
        }
    }
}

```



> 删除

```java
package com.f.lesson03;

import com.f.lesson02.utils.jdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestDelete {
    public static void main(String[] args) {
        Connection conn=null;
        PreparedStatement st=null;
        ResultSet rs=null;
        try {
            conn = jdbcUtils.getConnection();
            String sql="delete from users where id=?";
            st = conn.prepareStatement(sql);
            st.setInt(1,5);
            int i = st.executeUpdate();
            if(i>0){
                System.out.println("删除成功哦！");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            jdbcUtils.release(conn,st,null);
        }
    }
}

```



> 更新



> 查询



## 10.7 使用IDEA连接数据库

![image-20210520211217693](https://gitee.com/feng2828/image/raw/master/img/20210723144808.png)

![image-20210520211253613](https://gitee.com/feng2828/image/raw/master/img/20210723144809.png)

![img](https://img-blog.csdnimg.cn/20181119004716165.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zODIxNDE3MQ==,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20201015215512435.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzQ4NDUz,size_16,color_FFFFFF,t_70#pic_center)

![img](https://img-blog.csdnimg.cn/20201015215840605.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzQ4NDUz,size_16,color_FFFFFF,t_70#pic_center)

**2.也可以在url后添加：?serverTimezone=GMT**

![img](https://img-blog.csdnimg.cn/20201015220134373.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzQ4NDUz,size_16,color_FFFFFF,t_70#pic_center)

连接成功后，可以选择数据库

![image-20210520213208374](https://gitee.com/feng2828/image/raw/master/img/20210723144810.png)

双击数据库

![image-20210520213425010](https://gitee.com/feng2828/image/raw/master/img/20210723144811.png)

![image-20210520214136587](https://gitee.com/feng2828/image/raw/master/img/20210723144812.png)

![image-20210520214736179](https://gitee.com/feng2828/image/raw/master/img/20210723144813.png)

如果在IDEA中连接不上数据库，则在这里下载对应的驱动版本

![image-20210520215745377](https://gitee.com/feng2828/image/raw/master/img/20210723144814.png)

连接失败，查看原因



## 10.8 事务

`要么都成功，要么都失败`

> ACID原则

代码实现

1. 开启事务 ` conn.setAutoCommit(false);`
2. 一组业务执行完毕，提交事务
3. 可以在catch语句中现实定义的回滚语句，但是默认失败会回滚

```java
package com.f.lesson04;

import com.f.lesson02.utils.jdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestTransaction {
    public static void main(String[] args)  {
        Connection conn=null;
        PreparedStatement st =null;
        ResultSet rs=null;
        try {
            conn = jdbcUtils.getConnection();
            conn.setAutoCommit(false);//关闭数据库自动提交后，会自动开启事务，不用写开启事务了

            String sql1="update account set money=money-100 where name='A'";
            st = conn.prepareStatement(sql1);
            st.executeUpdate();

            String sql2="update account set money=money+100 where name='B'";
             st = conn.prepareStatement(sql2);
             st.executeUpdate();

             //业务完毕，提交事务
            conn.commit();
            System.out.println("提交成功！");


        } catch (SQLException throwables) {
            try {
                conn.rollback();//如果失败则回滚
                //这里默认会回滚，不写也行
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {
                jdbcUtils.release(conn,st,null);
            }
            throwables.printStackTrace();
        }
    }
}

```

## 10.9 数据库连接池

数据库连接-->执行完毕-->释放  

连接-->释放：十分浪费系统资源

**池化技术**

`准备一些预先的资源，过来就连接预先准备好的`



常用连接数 ——>设置**最小连接数**

最大连接数：业务最高承载上限

超过最大连接数的业务 **排队等待**

等待超时：100ms

 

`本质`：编写连接池，实现一个接口 DataSourse

> 开源数据源实现（拿来即用）

DBCP

C3P0

Druid:阿里巴巴



使用了这些数据库连接池之后，我们在项目中就不需要编写连接数据库的代码了。

> DBCP

需要用到的JAR包

commons-dpcp-..

commons-pool-...

包导好之后，便是properties文件的配置，下边给出了配置的详细属性

```file
driverClassName=com.mysql.jdbc.Driver  // 不多解释，这是基本的驱动加载

url=jdbc:mysql://localhost/db_student    // 驱动注册

username=root    //要连接的数据库用户名

password=root   // 要连接的数据库密码

defaultAutoCommit=true：// 设置是否自动提交,默认为true

defaultReadOnly=false： // 是否为只读 默认为false

defaultTransactionIsolation=3：// 设置数据库的事务隔离级别默认为1，READ_UNCOMMITTED，推荐设置为3

initialSize=10：  // 初始化数据池拥有的连接数量

maxActive=20：  /池中最多可容纳的活着的连接数量，当达到这个数量不在创建连接

maxIdle=20：  // 最大空闲等待，也就是连接等待队列超过这个值会自动回收未使用的连接，直到达到20

minIdle=5： // 最小空闲等待 ,数据池中最少保持的连接

maxWait=10000   // 最大等待时间，超过这个时间等待队列中的连接就会失效

testOnBorrow=true  //从池中取出连接时完成校验 ，验证不通过销毁这个connection，默认为true，

testOnReturn=false  //放入池中时完成校验，默认我fasle

validationQuery=select 1  // 校验语句，必须是查询语句，至少查询一列，设置了它onBorrow才会生效

validationQueryTimeout=1  // 校验查询时长，如果超过，认为校验失败

testWhileIdle=false   // 清除一个连接时是否需要校验

timeBetweenEvictionRunsMillis=1  // DBCP默认有个回收器Eviction，这个为设置他的回收时间周期

numTestsPerEvictionRun=3  // Eviction在运行时一次处理几个连接

poolPreparedStatements=true  //是否缓存PreparedStatements

maxOpenPreparedStatements=1 // 缓存PreparedStatements的最大个数
```



**dbcp连接池常用基本配置属性**
**initialSize** ：连接池启动时创建的初始化连接数量（默认值为0）

**maxActive** ：连接池中可同时连接的最大的连接数（默认值为8，调整为20，高峰单机器在20并发左右，自己根据应用场景定）

**maxIdle**：连接池中最大的空闲的连接数，超过的空闲连接将被释放，如果设置为负数表示不限制（默认为8个，maxIdle不能设置太小，因为假如在高负载的情况下，连接的打开时间比关闭的时间快，会引起连接池中idle的个数 上升超过maxIdle，而造成频繁的连接销毁和创建，类似于jvm参数中的Xmx设置)

**minIdle**：连接池中最小的空闲的连接数，低于这个数量会被创建新的连接（默认为0，调整为5，该参数越接近maxIdle，性能越好，因为连接的创建和销毁，都是需要消耗资源的；但是不能太大，因为在机器很空闲的时候，也会创建低于minidle个数的连接，类似于jvm参数中的Xmn设置）

**maxWai**t ：最大等待时间，当没有可用连接时，连接池等待连接释放的最大时间，超过该时间限制会抛出异常，如果设置-1表示无限等待（默认为无限，调整为60000ms，避免因线程池不够用，而导致请求被无限制挂起）

**poolPreparedStatements**：开启池的prepared（默认是false，未调整，经过测试，开启后的性能没有关闭的好。）

**maxOpenPreparedStatements**：开启池的prepared 后的同时最大连接数（默认无限制，同上，未配置）

**minEvictableIdleTimeMillis** ：连接池中连接，在时间段内一直空闲， 被逐出连接池的时间

**removeAbandonedTimeout** ：超过时间限制，回收没有用(废弃)的连接（默认为 300秒，调整为180）

**removeAbandoned** ：超过removeAbandonedTimeout时间后，是否进 行没用连接（废弃）的回收（默认为false，调整为true)

**连接工具包**

```java
package com.f.lesson05.Utils;

import org.apache.commons.dbcp.BasicDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public  class jdbcUtils_DPCP {
     private  static DataSource dataSource=null;
    static {
        try {
            InputStream in = jdbcUtils_DPCP.class.getClassLoader().getResourceAsStream("dpcpconfig.properties");
            Properties properties = new Properties();
            properties.load(in);

            //创建数据源  工厂模式——>创建对象
            dataSource = BasicDataSourceFactory.createDataSource(properties);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //2.获取连接
    public static Connection getConnection() throws SQLException {
         return dataSource.getConnection();//从数据源中获取连接

    }
    //3.释放连接资源
    public static void release(Connection conn, Statement st, ResultSet rs)  {
        if(rs!=null){
            try {
                rs.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if(st!=null){
            try {
                st.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}

```



> C3P0

需要用的jar包

![image-20210523134925828](https://gitee.com/feng2828/image/raw/master/img/20210723144815.png)

![image-20210523135200266](https://gitee.com/feng2828/image/raw/master/img/20210723144816.png)

![image-20210523135224409](https://gitee.com/feng2828/image/raw/master/img/20210723144817.png)

**连接类**

![image-20210523140810969](https://gitee.com/feng2828/image/raw/master/img/20210723144818.png)

> 结论

无论使用什么数据源，本质还是一样的，DataSourse接口不会变，方法就不会变



APACHE

![image-20210523141940716](https://gitee.com/feng2828/image/raw/master/img/20210723144819.png)











































